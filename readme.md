# Lumen PHP Framework

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://poser.pugx.org/laravel/lumen-framework/d/total.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/lumen-framework/v/stable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/lumen-framework/v/unstable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://poser.pugx.org/laravel/lumen-framework/license.svg)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## How To
sql server driver configuration for mac
https://github.com/Microsoft/msphpsql/tree/dev#install-unix

$ brew tap microsoft/msodbcsql https://github.com/Microsoft/homebrew-mssql-release  
$ brew update  
$ brew install unixodbc  
$ brew install msodbcsql   
$ brew install mssql-tools  
$ brew install llvm --with-clang --with-clang-extra-tools  
$ brew install autoconf  
$ sudo pear config-set php_ini php --ini | grep "Loaded Configuration" | sed -e "s|.*:\s*||"  

$ sudo pecl search sqlsrv // to search for the latest releases  
$ sudo pecl install sqlsrv-[version]  
$ sudo pecl install pdo_sqlsrv-[version]  

extension=pdo_pgsql.so  
extension=pdo_sqlsrv.so  


##SWAGGER
cd gameservice  
mkdir public/api  // to keep swagger.json  
cd app  
app tmrtrn$ ../vendor/bin/swagger --bootstrap constants.php --output ../public/api/  // generate   swagger.json  
http://localhost:8888/gameservice/public/swagger/index.html  



## Official Documentation

Documentation for the framework can be found on the [Lumen website](http://lumen.laravel.com/docs).

## Security Vulnerabilities

If you discover a security vulnerability within Lumen, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Lumen framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
