<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoomTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('room_type')->insert([
            'TYPE' => 0,
            'owner_name' => 'mynet',
            'visibility' => true,
        ]);
        DB::table('room_type')->insert([
            'TYPE' => 1,
            'owner_name' => 'vip',
            'visibility' => false,
        ]);
        DB::table('room_type')->insert([
            'TYPE' => 2,
            'owner_name' => 'vip',
            'visibility' => true,
        ]);
    }
}
