<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCustomDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('UserCustomData', function (Blueprint $table) {
            $table->string('USERNICK');
            $table->string('DataKey', 45);
            $table->string('DataValue');
            $table->primary(['USERNICK','DataKey']);
            $table->timestamps();
            $table->string('ProductSku')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('UserCustomData');
    }
}
