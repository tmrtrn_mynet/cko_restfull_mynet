<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAchievementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*CreateAchievementTable: 
        create table "Achievement" ("id" int identity primary key not null, "Code" nvarchar(100) not null, "Group" nvarchar(100) null, "DD_GAME_ID" nvarchar(255) not null, "Title" nvarchar(255) not null, "Description" nvarchar(255) not null, "UniqueRecords" tinyint not null default '0', "Position" int not null, "Interval" smallint not null default '0', "Uptime" smallint not nulldefault '0', "TargetPoint" int not null, "UpdateOnesInUptime" bit not null default '0', "Reward" nvarchar(255) null, "IsActive" bit not null default '1')
        */
        Schema::create('Achievement', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Code', 100);
            $table->string('Group', 100)->nullable(true);
            $table->string('DD_GAME_ID');
            $table->string('Title', 255);
            $table->string('Description', 255);
            $table->tinyInteger('UniqueRecords')->default(0);
            $table->integer('Position');
            $table->smallInteger('Interval')->default(0);
            $table->smallInteger('Uptime')->default(0);
            $table->integer('TargetPoint');
            $table->boolean('UpdateOnesInUptime')->default(false);
            $table->string('Reward')->nullable(true);
            $table->boolean('IsActive')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Achievement');
    }
}
