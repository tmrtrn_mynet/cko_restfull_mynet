<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAchievementUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
          create table "Achievement_User" ("id" bigint identity primary key not null, "IdAchievement" int not null, "USERNICK" nvarchar(255) not null, "CurrentPoint" int not null, "ExpiredAt" datetime null, "UpdatedAt" datetime null, "RewardClaimed" bit not null default '0')
        */
        Schema::create('Achievement_User', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('IdAchievement');
            $table->string('USERNICK');
            $table->integer('CurrentPoint');
            $table->dateTime('ExpiredAt')->nullable(true);
            $table->dateTime('UpdatedAt')->nullable(true);
            $table->boolean('RewardClaimed')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Achievement_User');
    }
}
