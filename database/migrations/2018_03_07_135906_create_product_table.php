<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->string('Sku');
            $table->string('Category')->nullable(false);
            $table->string('Name')->nullable(false);
            $table->text('Details')->nullable(true);
            $table->text('TermsAndConditions')->nullable(true);
            $table->string('PictureUrl', 250)->nullable(true);
            $table->boolean('IsActive')->default(true);
            $table->primary('Sku');
            $table->foreign('Category')->references('name')->on('product_category');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
