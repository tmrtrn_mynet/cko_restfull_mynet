<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductPurchasedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_purchased', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ProductSku');
            $table->string('BelongsTo')->nullable(false);
            $table->integer('Duration')->nullable(false);
            $table->timestamp('ExpireDate');
            $table->boolean('IsTrial')->default(false);
            $table->string('Owner');
            $table->string('Status')->default('Pending');
            $table->string('TransactionToken')->nullable(false);
            $table->integer('Quantity')->default(1);
            $table->text('CustomData');
            $table->float('PurchasePrice', 8, 2);
            $table->string('PurchasePriceCurrency', 5)->default('TRY');
            $table->string('PaymentType', 125);
            $table->foreign('ProductSku')->references('Sku')->on('product');
            $table->unique('TransactionToken');
            $table->timestamps();
            $table->timestamp('StartDate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_purchased');
    }
}
