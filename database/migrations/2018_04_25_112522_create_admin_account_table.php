<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        create table "AdminAccount" ("id" int identity primary key not null, "username" nvarchar(255) not null, 
        "password" nvarchar(255) not null)
        */
        Schema::create('AdminAccount', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username');
            $table->string('password');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('AdminAccount');
    }
}
