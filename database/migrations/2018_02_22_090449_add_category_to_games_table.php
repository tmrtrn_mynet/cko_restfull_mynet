<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryToGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('GAMES', function (Blueprint $table) {
            $table->string('category_id')->default('cko');
            $table->boolean('show_player_count')->default(false);
            $table->string('roomlist_url')->nullable();
            $table->string('playnow_url')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*
        Schema::table('GAMES', function (Blueprint $table) {
            //
        }); */
    }
}
