<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteWidgetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Site_Widget', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('page_id');
            $table->string('title', 100)->nullable();
            $table->json('customData');
            $table->timestamps();
            $table->foreign('page_id')->references('id')->on('Site_Page');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Site_Widget');
    }
}
