<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomCustomData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('RoomCustomData', function (Blueprint $table) {
            $table->integer('ROOMID');
            $table->string('DataKey', 45);
            $table->string('DataValue');
            $table->primary(['ROOMID','DataKey']);
            $table->timestamps();
            $table->string('ProductSku')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('RoomCustomData');
    }
}
