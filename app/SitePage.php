<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Swagger\Annotations as SWG;

/**
 * @SWG\Definition(
 *     @SWG\Property(property="name", type="string"),
 *     @SWG\Property(property="title", type="string"),
 *     @SWG\Property(property="url", type="string"),
 * )
 */
class SitePage extends Model {
  /**
   * The table associated with the model.
   *
   * @var string
   */

  protected $table = 'Site_Page';
  protected $dateFormat = 'Y-m-d H:i:s';
  protected $fillable = [ 'name', 'title', 'url' ];
}