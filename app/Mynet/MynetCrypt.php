<?php
namespace App\Mynet;
class MynetCrypt{
	
	var $identifier1 = "06092B0601040182375803A0";
	var $identifier2 = "060A2B060104018237580301A0";
	var $identifier3 = "020302000102026601020140";
	
	var $hexIV = "";
	var $hexSalt = "";
	var $hexData = "";
	var $blocksize = "";
	
	var $hexchar = Array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D','E', 'F');
	var $hexindex = "0123456789abcdef          ABCDEF";
	
	var $special_chars_input = array("slash","equals","plus");
	var $special_chars_output = array("/","=","+");	
	
	function decrypt($data){
		$encryptedHash = substr(str_replace($this->special_chars_input,$this->special_chars_output,$data),0,32);
		$encryptedData = base64_decode(substr(str_replace($this->special_chars_input,$this->special_chars_output,$data),32));
		if(!$encryptedHash && !$encryptedData)
			return false;
			
		$hexData = bin2hex($encryptedData);
		$this->parseEncryptedData($hexData);
		
		$this->blocksize = mcrypt_get_block_size(MCRYPT_DES, MCRYPT_MODE_CBC);
		$salt = $this->hex2bin($this->hexSalt);
		$iv = $this->hex2bin($this->hexIV);
		$data = $this->hex2bin($this->hexData);
		$pass = $this->toUnicodeHexString(env('MYNETCRYPT_KEY', 'mynet'));
		$key = $this->hex2bin(substr(sha1($this->hex2bin($this->encryptWithSHA1($pass,$this->hexSalt))),0,16));
		if(!$data && !$iv && !$key)
			return false;
		
		$dec_bin_data = mcrypt_decrypt(MCRYPT_DES,$key,$data,MCRYPT_MODE_CBC,$iv);
		$decrypted = $this->cleanBin($this->unpad($dec_bin_data,$this->blocksize));
		if($encryptedHash != strtoupper(md5($this->hex2bin($this->toUnicodeHexString($decrypted)))))
			return false;
		else
			return $decrypted;
	}
	
	function encrypt($data){
		$iv = $this->hex2bin($this->hexIV);
		$cipherText = $this->hex2bin($this->toUnicodeHexString($data));
		$pass = $this->toUnicodeHexString(env('MYNETCRYPT_KEY', 'mynet'));
		$key = $this->hex2bin(substr(sha1($this->hex2bin($this->encryptWithSHA1($pass,$this->hexSalt))),0,16));
		
		$edata = bin2hex(mcrypt_encrypt(MCRYPT_DES,$key,$this->pkcs5_pad($cipherText,$this->blocksize),MCRYPT_MODE_CBC,$iv));
		$edata = "04" . $this->getDataLengthString($edata) . $edata;
		$edata = "0410" . $this->hexSalt . $edata;
		$edata = $this->identifier3 . "0408" . $this->hexIV . $edata;
		$edata = "30" . $this->getDataLengthString($edata) . $edata;
		$edata = $this->identifier2 . $this->getDataLengthString($edata) . $edata;
		$edata = "30" . $this->getDataLengthString($edata) . $edata;
		$edata = $this->identifier1 . $this->getDataLengthString($edata) . $edata;
		$edata = "30" . $this->getDataLengthString($edata) . $edata;
		$hashed = strtoupper(md5($cipherText));
		return str_replace($this->special_chars_output,$this->special_chars_input,$hashed.base64_encode($this->hex2bin($edata)));
	}
	
	function pre_print($str){
		echo "<pre>";
		print_r($str);
		echo "</pre>";
	}
	
	function hex2bin($hexString){
		$hexLenght = strlen($hexString);
		if ($hexLenght % 2 != 0 || preg_match("/[^\da-fA-F]/",$hexString)) return false;
	
		$binString = "";
		for ($x = 1; $x <= $hexLenght/2; $x++){
			$binString .= chr(hexdec(substr($hexString,2 * $x - 2,2)));
		}
	
		return $binString;
	}

	function hex2byte($hexData){
		$l = strlen($hexData) / 2;
		$j = 0;
		for($i = 0; $i < $l; $i++){
			$c = $hexData[$j++];
			if(strstr($this->hexindex,$c)){
				$n = substr(strstr($this->hexindex,$c),0,1);
			} else {
				$n = -1;
			}
			$b = ($n & 0xf) << 4;
			$c = $hexData[$j++];
			if(strstr($this->hexindex,$c)){
				$n = substr(strstr($this->hexindex,$c),0,1);
			} else {
				$n = -1;
			}
			$b += ($n & 0xf);
			$data[$i] = $b;
		}
		return $data;
	}
	
	function longToHex($long){
		$str = dechex($long);
		if(strlen($str) % 2 != 0) return "0" . $str;
		else return $str;
	}
	
	function toUnicodeHexString($asciiData){
		$tmpHex = "";
		for($i = 0; $i < strlen($asciiData); $i++){
			$chr = (int) ord($asciiData[$i]);
			$tmpHex .= dechex($chr);
			$tmpHex .= "00";
		}
		return $tmpHex;
	}
	
	function getDataLengthAsByte($byteDataLength){
		if($byteDataLength <= 0 ){
			$byteDataLength = 1*2;
		} else {
			$byteDataLength += 1;
			$byteDataLength *= 2;
		}
		return $byteDataLength;
	}
	
	function getDataLengthString($data){
		$datalength = strlen($data)/2;
		if($datalength < 128){
			return $this->longToHex($datalength);
		} else {
			$len = 0;
			for($i = 1; $i < 128; $i++){
				$len = (int) (pow(256,$i) - 1);
				if($datalength > $len){
					continue;
				} else {
					return $this->longToHex(128 + $i) . "" . $this->longToHex($datalength);
				}
			}
			return "";
		}
	}
	
	function parseEncryptedData($encryptedData){
		$byteDataLength = 0;
		$str = substr($encryptedData,2,2);
		$byteDataLength = hexdec($str) - 128;
		$byteDataLength = $this->getDataLengthAsByte($byteDataLength);
		$str = substr($encryptedData,2 + $byteDataLength + 12*2);
		$str2 = substr($str,0,2);
		$byteDataLength = hexdec($str2) - 128;
		$byteDataLength = $this->getDataLengthAsByte($byteDataLength);
		$str = substr($str,$byteDataLength);
		$str2 = substr($str,2,2);
		$byteDataLength = hexdec($str2) - 128;
		$byteDataLength = $this->getDataLengthAsByte($byteDataLength);
		$str = substr($str,2 + $byteDataLength + 13*2);
		$str2 = substr($str,0,2);
		$byteDataLength = hexdec($str2) - 128;
		$byteDataLength = $this->getDataLengthAsByte($byteDataLength);
		$str = substr($str,$byteDataLength);
		$str2 = substr($str,2,2);
		$byteDataLength = hexdec($str2) - 128;
		$byteDataLength = $this->getDataLengthAsByte($byteDataLength);
		$str = substr($str,2 + $byteDataLength + 12*2);
		$this->hexIV = substr($str,4,16);
		$str = substr($str,20);
		$this->hexSalt = substr($str,4,32);
		$str = substr($str,36);
		$str2 = substr($str,2,2);
		$byteDataLength = hexdec($str2) - 128;
		$byteDataLength = $this->getDataLengthAsByte($byteDataLength);
		$str = substr($str,2 + $byteDataLength);
		$this->hexData = $str;
	}

	function encryptWithSHA1($password,$salt){
		$passwordPlusSalt = $password.$salt;
		return $passwordPlusSalt;
	}	

	function cleanBin($str){
		$out = "";
		for($i = 0; $i < strlen($str); $i++){
			if($i % 2 == 0)
				$out .= $str[$i];
		}
		return $out;
	}
	
	function unpad($text,$block){
		$packing = ord($text{strlen($text) - 1});
		if($packing and ($packing < $block)){
			for($P = strlen($text) - 1; $P >= strlen($text) - $packing; $P--){
				if(ord($text{$P}) != $packing){
					$packing = 0;
				}
			}
		}
		return substr($text,0,strlen($text) - $packing);
	}	
	
	function pkcs5_pad($text, $blocksize){
    	$pad = $blocksize - (strlen($text) % $blocksize);
    	return $text . str_repeat(chr($pad), $pad);
	}
	
}

?>