<?php

namespace App;

use App\Room;
use Illuminate\Database\Eloquent\Model;
use Swagger\Annotations as SWG;

/**
 * @SWG\Definition(
 *     @SWG\Property(property="SPONSOR_NAME", type="string"),
 *     @SWG\Property(property="SPONSOR_IMAGE_URL", type="string"),
 *     @SWG\Property(property="SPOSINFO", type="string"),
 *     @SWG\Property(property="MOBIL_GATEWAY_ADDRES", type="string"),
 *     @SWG\Property(property="MOBIL_GATEWAY_PORT", type="integer"),
 *     @SWG\Property(property="MOBIL_MAX_MEMBERS", type="integer"),
 * )
 */

class RoomMynet extends Room {

  /**
   * The table associated with the model.
   *
   * @var string
   */

  protected $table = 'ROOMS';

  public $incrementing = false;
  protected $fillable = [
    'DD_GAME_ID', 'ROOMID', 'ROOMNAME', 'PORT', 'PLAYER_COUNT', 'TYPE', 'ROOMNAMETR'
  ];

  public static $visibleDetails = ['SPONSOR_NAME', 'SPONSOR_IMAGE_URL', 'SPOSINFO', 'MOBIL_GATEWAY_ADDRES', 'MOBIL_GATEWAY_PORT', 'MOBIL_MAX_MEMBERS'];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [
    //'TYPE'
  ];

}