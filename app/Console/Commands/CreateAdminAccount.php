<?php

namespace App\Console\Commands;

use App\Models\AdminAccount;
use Illuminate\Console\Command;

class CreateAdminAccount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create an admin account';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $username = $this->ask('username');

        if (AdminAccount::where('username', $username)->exists()) {
            $this->error('Admin already exists!');
            exit;
        }

        $password = $this->ask('password');

        $admin = AdminAccount::create([
            'username' => $username,
            'password' => app('hash')->make($password),
        ]);

        $this->info('Success');
        $this->line('username: '.$username);
        $this->line('password: '.$password);
    }
}
