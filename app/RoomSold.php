<?php

namespace App;

use App\Room;
use Illuminate\Database\Eloquent\Model;
use Swagger\Annotations as SWG;

/**
 * @SWG\Definition(
 *     @SWG\Property(property="DESCRIPTION", type="string"),
 *     @SWG\Property(property="WELCOME_MESSAGE", type="string"),
 *     @SWG\Property(property="OWNER", type="string"),
 *     @SWG\Property(property="PAID_EXPIRE", type="string"),
 *     @SWG\Property(property="MARKET_ID", type="string"),
 *     @SWG\Property(property="WAITING_ADMIN", type="boolean"),
 *     @SWG\Property(property="NEW_ROOMNAME", type="string"),
 *     @SWG\Property(property="LMD_ROOMNAME", type="string"),
 *     @SWG\Property(property="LMD_PUBLICFLAG", type="string"),
 *     @SWG\Property(property="NEW_DESCRIPTION", type="string"),
 *     @SWG\Property(property="MYNET_OWNED", type="integer"),
 *     @SWG\Property(property="IS_DELETED", type="boolean"),
 *     @SWG\Property(property="WAS_REJECTED", type="boolean"),
 *     @SWG\Property(property="properties", type="string"),
 *     @SWG\Property(property="SPECIALVIP_EXPIRE", type="string"),
 *     @SWG\Property(property="specialvip_imagestatus", type="boolean"),
 *     @SWG\Property(property="specialvip_tinyimage", type="string"),
 *     @SWG\Property(property="specialvip_textcolor", type="string"),
 *     @SWG\Property(property="specialvip_version", type="integer"),
 *     @SWG\Property(property="specialvip_showOwnerName", type="boolean"),
 *     @SWG\Property(property="LMD_SCORE_MULTIPLIER", type="string"),
 *     @SWG\Property(property="NEW_WELCOME_MSG", type="string"),
 *     @SWG\Property(property="city", type="string"),
 *     @SWG\Property(property="POPULATION_EXPIRE", type="string"),
 *     @SWG\Property(property="starterPack", type="boolean"),
 *     @SWG\Property(property="TRNV_ID", type="integer"),
 * )
 */

class RoomSold extends Room {

  /**
   * The table associated with the model.
   *
   * @var string
   */

  protected $table = 'sold_rooms';

  public $incrementing = false;
  protected $fillable = [
    'DD_GAME_ID', 'ROOMID', 'ROOMNAME', 'PORT', 'PLAYER_COUNT', 'TYPE', 'ROOMNAMETR'
  ];

  public static $visibleDetails = ['DESCRIPTION','WELCOME_MESSAGE','OWNER', 'PAID_EXPIRE', 'MARKET_ID', 'WAITING_ADMIN',
    'NEW_ROOMNAME', 'LMD_ROOMNAME', 'LMD_PUBLICFLAG', 'NEW_DESCRIPTION', 'MYNET_OWNED', 'IS_DELETED', 'WAS_REJECTED', 'properties',
    'SPECIALVIP_EXPIRE', 'specialvip_imagestatus', 'specialvip_tinyimage', 'specialvip_textcolor', 'specialvip_version', 'specialvip_showOwnerName',
    'LMD_SCORE_MULTIPLIER', 'NEW_WELCOME_MSG', 'city', 'POPULATION_EXPIRE', 'starterPack', 'TRNV_ID'];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [
    //'TYPE'
  ];

}