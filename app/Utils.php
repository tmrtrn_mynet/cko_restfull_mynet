<?php

namespace App;

class Utils {
  public static function GetDateTimeZoneUTC() {
    return new \DateTimeZone("Etc/UTC");
  }
  
  public static function GetCurrentDateTime($timezone = NULL) {
    if (!$timezone) {
        $timezone = self::GetDateTimeZoneUTC();
    }
    return new \DateTime("now", $timezone);
  }

  public static function GetDateTime($date, $timezone = NULL) {
    if (!$timezone) {
      $timezone = self::GetDateTimeZoneUTC();
  }
  return new \DateTime($date, $timezone);
  }
  
  public static function GetCurrentDateTimeFormat($format = "Y-m-d H:i:s", $timezone = NULL) {
    $now = self::GetCurrentDateTime($timezone);
    return $now->format($format);
  }

  public static function GetDiffHours($date1, $date2) {
    $diff = $date2->diff($date1);
    $hours = $diff->h;
    return $hours + ($diff->days * 24);
  }

  public static function CheckWebsericeSecurity() {
    $timestamp = (isset($_REQUEST["sig_time"]) ? stripslashes($_REQUEST["sig_time"]) : "");
    // Build the signature value that is expected
    $test = "";
    // First add the request timestamp
    $test .= $timestamp;
    $test .= env("APP_KEY");
    // Encrypt the expected signature
    $test = sha1($test);
    // Compare the received checksum signature to what we expect
    $signature = (isset ($_REQUEST["sig_crc"]) ? stripslashes ($_REQUEST["sig_crc"]) : "");
    if (strcmp($test, $signature) == 0) {
      return TRUE;
    }
    die ("Unauthorized request");
  }
}