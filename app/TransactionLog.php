<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Swagger\Annotations as SWG;

/**
 * @SWG\Definition(
 *     @SWG\Property(property="ADD_DATE", type="string"),
 *     @SWG\Property(property="MMO_ID", type="int"),
 *     @SWG\Property(property="MARKET_ID", type="string"),
 *     @SWG\Property(property="USERNICK", type="string"),
 *     @SWG\Property(property="EID", type="string"),
 *     @SWG\Property(property="PRODUCT", type="string"),
 *     @SWG\Property(property="PAYMENT_TYPE", type="integer"),
 *     @SWG\Property(property="QUANTITY", type="integer"),
 *     @SWG\Property(property="RESULT", type="string"),
 *     @SWG\Property(property="ROOM_ID", type="integer"),
 * )
 */
class TransactionLog extends Model {
  /**
   * The table associated with the model.
   *
   * @var string
   */

  protected $table = 'TRANSACTION_LOG';
  protected $primaryKey = 'ID';
  protected $dateFormat = 'Y-m-d H:i:s';
  protected $visible = [ 'ADD_DATE', 'MMO_ID', 'MARKET_ID', 'USERNICK', 'EID', 'PRODUCT', 'PAYMENT_TYPE', 'QUANTITY', 'RESULT', 'ROOM_ID' ];
/*
  public function scopeLatest($query)
  {
    return $query->sortBy('ADD_DATE', 'desc')->first();
  } */
}