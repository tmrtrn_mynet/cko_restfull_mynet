<?php
namespace App\Models;
use Illuminate\Auth\Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;

class AdminAccount extends Model implements AuthenticatableContract, JWTSubject
{

    use Authenticatable;

    protected $table = 'AdminAccount';
    public $timestamps = false;
    protected $fillable = ['username', 'password'];
    protected $hidden = ['password'];


    // jwt
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    // jwt
    public function getJWTCustomClaims()
    {
        return [];
    }
}