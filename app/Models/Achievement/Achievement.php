<?php
namespace App\Models\Achievement;

use Illuminate\Database\Eloquent\Model;
use Swagger\Annotations as SWG;

/**
 * @SWG\Definition(
 *     @SWG\Property(property="Code", type="string"),
 *     @SWG\Property(property="Group", type="string"),
 *     @SWG\Property(property="DD_GAME_ID", type="string"),
 *     @SWG\Property(property="Title", type="string"),
 *     @SWG\Property(property="Description", type="string"),
 *     @SWG\Property(property="UniqueRecords", type="integer"),
 *     @SWG\Property(property="Position", type="integer"),
 *     @SWG\Property(property="Interval", type="integer"),
 *     @SWG\Property(property="Uptime", type="integer"),
 *     @SWG\Property(property="TargetPoint", type="integer"),
 *     @SWG\Property(property="UpdateOnesInUptime", type="boolean"),
 *     @SWG\Property(property="Reward", type="string"),
 *     @SWG\Property(property="IsActive", type="boolean"),
 * )
 */
class Achievement extends Model {
  protected $table = 'Achievement';
  protected $primaryKey = 'Code';
  protected $keyType = 'string';
  public $timestamps = false;
  protected $dateFormat = 'Y-m-d H:i:s';
  protected $fillable = ['Code', 'Group', 'DD_GAME_ID', 'Title', 'Description', 'UniqueRecords', 'Position', 'Interval', 'Uptime', 'TargetPoint', 'UpdateOnesInUptime', 'Reward', 'IsActive'];
  protected $visible = ['Code', 'Group', 'DD_GAME_ID', 'Title', 'Description', 'UniqueRecords', 'Position', 'Interval', 'Uptime', 'TargetPoint', 'Reward', 'UpdateOnesInUptime', 'IsActive'];
}
