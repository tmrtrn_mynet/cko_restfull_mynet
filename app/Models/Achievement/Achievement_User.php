<?php
namespace App\Models\Achievement;

use Illuminate\Database\Eloquent\Model;
use Swagger\Annotations as SWG;

/**
 * @SWG\Definition(
 *     @SWG\Property(property="IdAchievement", type="integer"),
 *     @SWG\Property(property="USERNICK", type="string"),
 *     @SWG\Property(property="CurrentPoint", type="integer"),
 *     @SWG\Property(property="ExpiredAt", type="string"),
 *     @SWG\Property(property="UpdatedAt", type="string"),
 *     @SWG\Property(property="RewardClaimed", type="boolean"),
 * )
 */
class Achievement_User extends Model {
  protected $table = 'Achievement_User';
  public $timestamps = false;
  protected $dateFormat = 'Y-m-d H:i:s';
  protected $fillable = ['IdAchievement', 'USERNICK', 'CurrentPoint', 'ExpiredAt', 'UpdatedAt', 'RewardClaimed'];
  protected $visible = ['IdAchievement', 'USERNICK', 'CurrentPoint', 'ExpiredAt', 'UpdatedAt', 'RewardClaimed'];

  public function achievement() {
    return $this->hasOne('App\Models\Achievement\Achievement', 'id', 'IdAchievement');
  }
}
