<?php
namespace App\Models;
use Illuminate\Auth\Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
class User extends Model implements AuthenticatableContract, JWTSubject
{

    use Authenticatable;

    protected $table = 'MEMBERS';
    protected $primaryKey = 'USERNICK';
    protected $keyType = 'string';
    protected $dateFormat = 'Y-m-d H:i:s';
    public $timestamps = false;
    protected $fillable = ['USERNICK'];


    public function user_achievements() {
        return $this->hasMany(Achievement\Achievement_User::class, 'USERNICK', 'USERNICK');
    }

    // jwt
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    // jwt
    public function getJWTCustomClaims()
    {
        return [];
    }
}