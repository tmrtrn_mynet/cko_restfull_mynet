<?php
namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;
use Swagger\Annotations as SWG;
use App\Utils;

class User extends Model {
/**
 * The table associated with the model.
 *
 * @var string
 */
  protected $table = 'MEMBERS';
  protected $primaryKey = 'USERNICK';
  protected $keyType = 'string';
  protected $dateFormat = 'Y-m-d H:i:s';

  protected $visible = [ 'USERNICK', 'isSuper', 'SUPERNICK', 'USE_SUPERNICK'];
  protected $appends = array('isSuper');


  public function getIsSuperAttribute()
  {
      return $this->NEW_SUPER_EXPIRE > Utils::GetCurrentDateTimeFormat('Y-m-d H:i:s');
  }
}