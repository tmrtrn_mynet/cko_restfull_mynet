<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Swagger\Annotations as SWG;

/**
 * @SWG\Definition(
 *     @SWG\Property(property="USERNICK", type="string"),
 *     @SWG\Property(property="DD_GAME_ID", type="string"),
 *     @SWG\Property(property="DD_LEVEL_ID", type="string"),
 *     @SWG\Property(property="TOTAL_SCORE", type="integer"),
 *     @SWG\Property(property="GAMES_WON", type="integer"),
 *     @SWG\Property(property="GAMES_LOST", type="integer"),
 *     @SWG\Property(property="GAME_EXPERIENCE", type="integer"),
 *     @SWG\Property(property="ABONDONED_GAMES", type="integer"),
 *     @SWG\Property(property="GAMES_DRAWN", type="integer"),
 *     @SWG\Property(property="LAST_DAY", type="string"),
 *     @SWG\Property(property="LAST_DAY_SCORE", type="integer"),
 *     @SWG\Property(property="LAST_DAY_ACTIVITY", type="integer"),
 *     @SWG\Property(property="SINGLE_MODE_SCORE", type="integer"),
 *     @SWG\Property(property="SINGLE_MODE_LAST_PLAYED", type="string"),
 * )
 */
class OverallScores extends BaseModel {
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'OVERALL_SCORES';
  public $incrementing = false;
  public $timestamps = false;
  protected $fillable = [ 'USERNICK', 'DD_GAME_ID', 'DD_LEVEL_ID', 'TOTAL_SCORE',
    'GAMES_WON', 'GAMES_LOST', 'GAME_EXPERIENCE', 'ABONDONED_GAMES', 'GAMES_DRAWN',
    'LAST_DAY', 'LAST_DAY_SCORE', 'LAST_DAY_ACTIVITY', 'SINGLE_MODE_SCORE', 'SINGLE_MODE_LAST_PLAYED'];

    public function user()
    {
        return $this->belongsTo(User::class, 'USERNICK', 'USERNICK');
    }
}