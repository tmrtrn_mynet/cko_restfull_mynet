<?php
namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Swagger\Annotations as SWG;

/**
 * @SWG\Definition(
 *     @SWG\Property(property="groupId", type="integer"),
 *     @SWG\Property(property="name", type="string"),
 *     @SWG\Property(property="parentCategory", type="string"),
 *     @SWG\Property(property="description", type="string"),
 * )
 */
class ProductCategory extends Model {
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'product_category';
  protected $primaryKey = 'name';
  protected $keyType = 'string';
  public $incrementing = false;
  public $timestamps = false;
  protected $fillable = [ 'groupId', 'name', 'parentCategory', 'description'];
}