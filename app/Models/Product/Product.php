<?php
namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Swagger\Annotations as SWG;

/**
 * @SWG\Definition(
 *     @SWG\Property(property="Sku", type="string"),
 *     @SWG\Property(property="Category", type="string"),
 *     @SWG\Property(property="Name", type="string"),
 *     @SWG\Property(property="Details", type="string"),
 *     @SWG\Property(property="TermsAndConditions", type="string"),
 *     @SWG\Property(property="PictureUrl", type="string"),
 *     @SWG\Property(property="IsActive", type="boolean"),
 * )
 */
class Product extends Model {
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'product';
  protected $primaryKey = 'Sku';
  protected $keyType = 'string';
  protected $dateFormat = 'Y-m-d H:i:s';
  protected $fillable = [ 'Sku', 'Category', 'Name', 'Details', 'TermsAndConditions', 'PictureUrl', 'IsActive'];
  protected $visible = [ 'Sku', 'Category', 'Name', 'Details', 'TermsAndConditions', 'PictureUrl', 'IsActive'];

  public function scopeSearch($query, $keywords)
  {
      return $query->where('Sku', 'LIKE', '%'.$keywords.'%');
  }

  public function scopeOfSku($query, $sku)
  {
      return $query->where('Sku', $sku);
  }

  public function scopeOfActive($query, $isActive) {
    return $query->where('IsActive', $isActive);
  }

  /**
   * Get type properties of room
   */
  public function Category()
  {
      return $this->belongsTo('App\Models\Product\ProductCategory', 'Category', 'name');
  }

}