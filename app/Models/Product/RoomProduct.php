<?php
namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Swagger\Annotations as SWG;

/**
 * @SWG\Definition(
 *     @SWG\Property(property="ProductPurchasedId", type="integer"),
 *     @SWG\Property(property="RoomId", type="integer"),
 *     @SWG\Property(property="Quantity", type="integer"),
 *     @SWG\Property(property="CustomData", type="string"),
 * )
 */
class RoomProduct extends Model {
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'room_product';
  protected $dateFormat = 'Y-m-d H:i:s';
  protected $fillable = ['ProductPurchasedId', 'RoomId', 'Quantity', 'CustomData'];
  // protected $attributes = ['ProductPurchasedId' => 33];

  /**
   * Get the purchased item record associated with the room product.
   */
  public function purchases()
  {
      return $this->hasOne('App\Models\Product\ProductPurchased', 'id', 'ProductPurchasedId');
  }
}