<?php
namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Swagger\Annotations as SWG;

/**
 * @SWG\Definition(
 *     @SWG\Property(property="ProductSku", type="string"),
 *     @SWG\Property(property="BelongsTo", type="string"),
 *     @SWG\Property(property="Duration", type="integer"),
 *     @SWG\Property(property="ExpireDate", type="string"),
 *     @SWG\Property(property="IsTrial", type="boolean"),
 *     @SWG\Property(property="Owner", type="string"),
 *     @SWG\Property(property="Status", type="string"),
 *     @SWG\Property(property="TransactionToken", type="string"),
 *     @SWG\Property(property="PurchasePrice", type="float"),
 *     @SWG\Property(property="PurchasePriceCurrency", type="string"),
 *     @SWG\Property(property="PaymentType", type="string"),
 *     @SWG\Property(property="StartDate", type="string"),
 * )
 */
class ProductPurchased extends Model {
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'product_purchased';
  public $dateFormat = 'Y-m-d H:i:s';
  protected $fillable = [ 'ProductSku','BelongsTo', 'Duration', 'ExpireDate', 'IsTrial', 'Owner', 'Status', 'TransactionToken', 'Quantity', 'CustomData', 'PurchasePrice', 'PurchasePriceCurrency', 'PaymentType', 'StartDate'];

  public function scopeSearch($query, $keywords)
  {
      return $query->where('TransactionToken', 'LIKE', '%'.$keywords.'%');
  }

  public function product() {
    return $this->hasOne('App\Models\Product\Product', 'Sku', 'ProductSku');
  }

  public function scopeValid($query)
  {
    // $newDate = $oldDate->addDays($numDays);
      return $query->where('Status', 'Confirmed');
  }

}