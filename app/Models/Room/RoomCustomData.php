<?
namespace App\Models\Room;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Swagger\Annotations as SWG;

/**
 * @SWG\Definition(
 *     @SWG\Property(property="ROOMID", type="integer"),
 *     @SWG\Property(property="DataKey", type="string"),
 *     @SWG\Property(property="DataValue", type="string"),
 *     @SWG\Property(property="ProductSku", type="string"),
 * )
 */
class RoomCustomData extends Model {
  protected $table = 'RoomCustomData';
  protected $dateFormat = 'Y-m-d H:i:s';
  protected $primarykey = array('ROOMID', 'DataKey');
  public $incrementing = false;
  protected $fillable = [ 'ROOMID', 'DataKey', 'DataValue', 'ProductSku'];
  protected $hidden = ['created_at', 'updated_at'];

  protected function setKeysForSaveQuery(Builder $query)
    {
        foreach ($this->primarykey as $key) {
            // UPDATE: Added isset() per devflow's comment.
            if (isset($this->$key))
                $query->where($key, '=', $this->$key);
            else
                throw new Exception(__METHOD__ . 'Missing part of the primary key: ' . $key);
        }

        return $query;
    }

    // UPDATE: From jessedp. See his edit, below.
    /**
     * Execute a query for a single record by ID.
     *
     * @param  array  $ids Array of keys, like [column => value].
     * @param  array  $columns
     * @return mixed|static
     */
    public static function find($ids, $columns = ['*'])
    {
        $me = new self;
        $query = $me->newQuery();
        foreach ($me->getKeyName() as $key) {
            $query->where($key, '=', $ids[$key]);
        }
        return $query->first($columns);
    }
}