<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LuckyBox extends BaseModel {
  protected $table = 'lucky_box';
  public $incrementing = false;
  public $timestamps = false;
  protected $primaryKey = 'ID';

  protected $fillable = ['usernick', 'create_date', 'click_date', 'award', 'paid', 'market_id'];
  protected $visible = ['ID', 'usernick', 'create_date', 'click_date', 'award', 'paid', 'market_id'];
}