<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Swagger\Annotations as SWG;

/**
 * @SWG\Definition(
 *     @SWG\Property(property="page_id", type="integer"),
 *     @SWG\Property(property="title", type="string"),
 *     @SWG\Property(property="customData", type="string"),
 *     @SWG\Property(property="created_at", type="string"),
 *     @SWG\Property(property="updated_at", type="string"),
 * )
 */
class SiteWidget extends Model {
  /**
   * The table associated with the model.
   *
   * @var string
   */

  protected $table = 'Site_Widget';
  protected $dateFormat = 'Y-m-d H:i:s';
  protected $fillable = [ 'page_id', 'title', 'customData' ];
}