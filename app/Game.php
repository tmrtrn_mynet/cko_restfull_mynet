<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Swagger\Annotations as SWG;

/**
 * @SWG\Definition(
 *     @SWG\Property(property="DD_GAME_ID", type="string", readOnly=true),
 *     @SWG\Property(property="GAMENAME", type="string"),
 *     @SWG\Property(property="CLASS_REFERENCE", type="string"),
 *     @SWG\Property(property="category_id", type="string"),
 *     @SWG\Property(property="PLAYERS_NEEDED", type="integer"),
 *     @SWG\Property(property="show_player_count", type="boolean"),
 *     @SWG\Property(property="roomlist_url", type="string"),
 *     @SWG\Property(property="playnow_url", type="string"),
 * )
 */
class Game extends Model
{
  public $timestamps = false;
  protected $primaryKey = 'DD_GAME_ID';
  protected $keyType = 'string';
  public $incrementing = false;

 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'DD_GAME_ID', 'GAMENAME', 'CLASS_REFERENCE', 'category_id','PLAYERS_NEEDED', 'show_player_count', 'roomlist_url', 'playnow_url'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}