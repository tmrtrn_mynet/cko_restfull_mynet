<?php

namespace App\Http\Controllers\User;

use App\Models\User\User;
use App\Models\User\UserCustomData;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use App\Utils;
use Swagger\Annotations as SWG;



/**
 * @SWG\Get(
 * 		path="/users",
 * 		tags={"users"},
 * 		operationId="showAll",
 * 		summary="Fetch users",
 *    @SWG\Parameter(
 * 	   name="perPage",
 *     in="query",
 * 		 type="integer",
 * 		 description="results per page, pagination",
 *     required=false,
 * 	 ),
 *   @SWG\Parameter(
 * 	   name="usernick",
 *     in="query",
 * 		 type="string",
 *     required=false,
 * 		 description="search by username",
 * 	 ),
 *   @SWG\Parameter(
 * 	   name="use_supernick",
 *     in="query",
 * 		 type="integer",
 *     required=false,
 * 		 description="1 or 0 user chhosed to show supernick or not",
 * 	 ),
 *   @SWG\Parameter(
 * 	   name="isSuper",
 *     in="query",
 * 		 type="integer",
 *     required=false,
 * 		 description="1 or 0",
 * 	 ),
 * 		@SWG\Response(
 * 			status=200,
 * 			description="success",
 * 			@SWG\Schema(
 *              @SWG\Property(property="data", ref="#/definitions/Room")
 *          ),
 * 		),
 * 		@SWG\Response(
 * 			status="default",
 * 			description="error",
 * 			@SWG\Schema(ref="#/definitions/Error"),
 * 		),
 * 	)
 *
 */
class UserController extends Controller {

  public function showAll(Request $request)
  {
    $pp = 15;
    if ($request->has('perPage')) {
      $pp = $request->perPage;
    }
    $searchUser = "";
    if ($request->has('usernick')) {
     $searchUser = $request->usernick;
    }
    $query = User::where('USERNICK', 'LIKE', '%' . $searchUser . '%');
    if ($request->has('use_supernick')) {
      $query->where('USE_SUPERNICK', $request->use_supernick);
    }
    if ($request->has('isSuper')) {
      if ($request->isSuper == 1) {
        $query->whereDate('NEW_SUPER_EXPIRE', '>', Utils::GetCurrentDateTimeFormat('Y-m-d H:i:s'));
      } else {
        $query->whereDate('NEW_SUPER_EXPIRE', '<', Utils::GetCurrentDateTimeFormat('Y-m-d H:i:s'));
      }
    }
    $users = $query->paginate($pp);
    return response()->json($users, 200, [], JSON_UNESCAPED_UNICODE);
  }


/**
 * @SWG\Get(
 * 		path="/users/{username}/customData",
 * 		tags={"users"},
 * 		operationId="getCustomData",
 * 		summary="Get Custom Data Value By Key",
 * 		@SWG\Parameter(
 * 			name="username",
 * 			in="path",
 * 			required=true,
 * 			type="string",
 * 			description="username",
 * 		),
 *    @SWG\Parameter(
* 	   name="keys",
*      in="query",
* 		 type="string",
*      required=true,
* 		 description="Data Keys which requested ex: SelectedIstaka,BlockedGuests",
* 	 ),
 * 		@SWG\Response(
 * 			status=200,
 * 			description="success",
 * 			@SWG\Schema(
 *              @SWG\Property(property="data", ref="#/definitions/UserCustomData"),
 *          ),
 * 		),
 * 		@SWG\Response(
 * 			status="default",
 * 			description="error",
 * 			@SWG\Schema(ref="#/definitions/Error"),
 * 		),
 * 	)
 *
 */
  public function getCustomData($username, Request $request) {
    $success = false;
    $message = "";
    try {
      if (!$request->has(['keys'])) {
        return response()->json([
          'success' => $success,
          'status' => 500,
          'message' => 'missing parameter',
        ]);
      }
      $success = true;
      $keysArr = explode(',',$request->keys);
      $message = UserCustomData::where('USERNICK', $username)->whereIn("DataKey", $keysArr)->get();

    } catch(\Illuminate\Database\QueryException $ex) {
      $success = false;
      $message = "ex " . $ex->getMessage();
    }
    return response()->json([
      'success' => $success,
      'status' => $success ? 200 : 500,
      'message' => $message,
    ]);
  }

  /**
 * @SWG\Post(path="/users/{username}/customData",
 *   tags={"users"},
 *   summary="Sets custom data for selected user with key-value",
 *   description="Keys must be unique",
 *   operationId="setCustomData",
 *   produces={"application/xml", "application/json"},
 *   @SWG\Parameter(
* 	   name="username",
*      in="path",
* 		 type="string",
*      required=true,
* 		 description="Selected username",
* 	 ),
*    @SWG\Parameter(
* 	   name="keys",
*      in="query",
* 		 type="string",
*      required=true,
* 		 description="Data keys, Should be CamelCase please ex: SelectedIstaka,BlockedGuests",
* 	 ),
*    @SWG\Parameter(
* 	   name="values",
*      in="query",
* 		 type="string",
*      required=true,
* 		 description="Data Values, ex: vipistaka, true",
* 	 ),
 *   @SWG\Response(response=400, description="Invalid input supplied"),
 *   @SWG\Response(response=404, description="UserCustomData not found")
 * )
 */
  public function setCustomData($username, Request $request) {
    $success = false;
    $message = "";
    if (!$request->has(['keys', 'values'])) {
      return response()->json([
        'success' => $success,
        'status' => 500,
        'message' => 'missing parameter',
      ]);
    }
    try {

      $keysArr = explode(',',$request->keys);
      $valuesArr = explode(',',$request->values);
      for ($i = 0; $i < count($keysArr); $i++) {
        $customData = UserCustomData::firstOrNew(
          ['USERNICK' => $username, 'DataKey' => $keysArr[$i]]
        );
        $customData->DataValue = $valuesArr[$i];
        $customData->save();
      }
      $success = true;

    } catch(\Illuminate\Database\QueryException $ex) {
      $success = false;
      $message = "ex " . $ex->getMessage();
    }
    return response()->json([
      'success' => $success,
      'status' => $success ? 200 : 500,
      'message' => $message,
    ]);

  }

}
