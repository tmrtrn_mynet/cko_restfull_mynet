<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Swagger\Annotations as SWG;

/**
 * @SWG\Swagger(
 *   @SWG\Info(
 *     title="Mynet Oyun swagger documented API",
 *     version="1.0.0",
 *   ),
 *   @swg\SecurityScheme(
 *      securityDefinition="Admin_Bearer",
 *      type="apiKey",
 *      in="header",
 *      name="Authorization",
 *      description="Auth Bearer Token Format as 'Bearer <access_token>'",
 *  ),
 *  @swg\SecurityScheme(
 *      securityDefinition="User_Bearer",
 *      type="apiKey",
 *      in="header",
 *      name="Authorization",
 *      description="Auth Bearer Token Format as 'Bearer <access_token>'",
 *  ),
 * )
 */
/**
 * @SWG\Swagger(
 *   host=API_HOST
 * )
 */
class Controller extends BaseController
{
    //
}
