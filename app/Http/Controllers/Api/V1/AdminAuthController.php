<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Models\Authorization;
use App\Models\AdminAccount;
use App\Transformers\AuthorizationTransformer;
use Swagger\Annotations as SWG;

class AdminAuthController extends BaseController
{

    protected $guard = 'admin';
  /*  public function __construct()
    {
        $this->middleware('auth:'.$this->guard, ['except' => ['store']]);
    }
    */

    /**
    * @SWG\Post(path="/admin_authorizations",
    *   tags={"admin"},
    *   summary="Loggin as Admin",
    *   description="Generates authentication token",
    *   operationId="store",
    *   produces={"application/xml", "application/json"},
    *   @SWG\Parameter(
    * 	   name="username",
    *     in="query",
    * 	   type="string",
    *     required=true,
    * 	   description="admin username",
    * 	 ),
    *   @SWG\Parameter(
    * 	   name="password",
    *     in="query",
    * 	   type="string",
    *     required=true,
    * 	   description="admin password",
    * 	 ),
    *   @SWG\Response(response=400, description="Invalid id supplied"),
    *   @SWG\Response(response=404, description="not found")
    * )
    */
    public function store(Request $request)
    {
      $validator = \Validator::make($request->all(), [
          'username' => 'required',
          'password' => 'required',
      ]);
      if ($validator->fails()) {
          return $this->errorBadRequest($validator);
      }
      $credentials = $request->only('username', 'password');
      
      if (! $token = $this->guard()->attempt($credentials)) {
          $this->response->errorUnauthorized(trans('auth.incorrect'));
      }
      $authorization = new Authorization($token);
      return $this->response->item($authorization, new AuthorizationTransformer())
          ->setStatusCode(201)->header('Authorization', 'Bearer ' . $token);
    }

    /**
    * @SWG\Put(path="/admin_authorizations/current",
    *   tags={"admin"},
    *   summary="Updates admin token",
    *   description="This can only be done by the logged in admin.",
    *   operationId="update",
    *   produces={"application/xml", "application/json"},
    *   @SWG\Response(response=400, description="Invalid token supplied"),
    *   @SWG\Response(response=404, description="PurchasedItem not found"),
    *   security={
    *       {"Admin_Bearer": {}}
    *   }
    * )
    */
    public function update()
    {
        $authorization = new Authorization($this->guard()->refresh());

        return $this->response->item($authorization, new AuthorizationTransformer());
    }

    /**
    * @SWG\Delete(path="/admin_authorizations/current",
    *   tags={"admin"},
    *   summary="Deletes admin token",
    *   description="This can only be done by the logged in admin.",
    *   operationId="destroy",
    *   produces={"application/xml", "application/json"},
    *   @SWG\Response(response=400, description="Invalid token supplied"),
    *   @SWG\Response(response=404, description="PurchasedItem not found"),
    *   security={
    *       {"Admin_Bearer": {}}
    *   }
    * )
    */
    public function destroy()
    {
        $this->guard()->logout();

        return $this->response->noContent();
    }

}