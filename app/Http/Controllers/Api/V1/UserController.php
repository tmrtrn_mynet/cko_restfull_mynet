<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Authorization;
use App\Transformers\UserTransformer;
use App\Serializers\NoDataArraySerializer;

class UserController extends BaseController
{
    /**
     * @SWG\Get(path="/users",
     *   tags={"users"},
     *   summary="user list",
     *   description="gets user list",
     *   operationId="index",
     *   produces={"application/xml", "application/json"},
     *   @SWG\Parameter(
     * 	   name="loginState",
     *     in="query",
     * 	   type="string",
     *     required=true,
     * 	   description="mynet user login state",
     * 	 ),
     *   @SWG\Response(response=400, description="Invalid id supplied"),
     *   @SWG\Response(response=404, description="not found"),
     * )
     */
    public function index(User $user)
    {
        $users = User::paginate();

        return $this->response->paginator($users, new UserTransformer());
    }


    /**
     * @SWG\Get(
     * 		path="/users/{id}",
     * 		tags={"users"},
     * 		operationId="show",
     * 		summary="some user's info",
     * 		@SWG\Parameter(
     * 			name="id",
     * 			in="path",
     * 			required=true,
     * 			type="string",
     * 			description="username",
     * 		),
     * 		@SWG\Response(
     * 			status=200,
     * 			description="success",
     * 			@SWG\Schema(
     *        @SWG\Property(property="data", ref="#/definitions/User")
     *      ),
     * 		),
     * 		@SWG\Response(
     * 			status="default",
     * 			description="error",
     * 			@SWG\Schema(ref="#/definitions/Error"),
     * 		)
     * 	)
     *
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        return $this->response->item($user, new UserTransformer(), [], function ($resource, $fractal) {
            $fractal->setSerializer(new NoDataArraySerializer());
        });
    }

    /**
     * @SWG\Get(
     * 		path="/user",
     * 		tags={"users"},
     * 		operationId="userShow",
     * 		summary="active user's info",
     * 		@SWG\Response(
     * 			status=200,
     * 			description="success",
     * 			@SWG\Schema(
     *        @SWG\Property(property="data", ref="#/definitions/User")
     *      ),
     * 		),
     * 		@SWG\Response(
     * 			status="default",
     * 			description="error",
     * 			@SWG\Schema(ref="#/definitions/Error"),
     * 		),
     *      security={
     *          {"User_Bearer": {}}
     *      }
     * 	)
     *
     */
    public function userShow()
    {
        return $this->response->item($this->user(), new UserTransformer(), [], function ($resource, $fractal) {
            $fractal->setSerializer(new NoDataArraySerializer());
        });
    }

    
}
