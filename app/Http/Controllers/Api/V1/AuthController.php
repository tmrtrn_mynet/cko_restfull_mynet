<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Models\Authorization;
use App\Models\User;
use App\Transformers\AuthorizationTransformer;
use App\Mynet\LoginState;
use Swagger\Annotations as SWG;

class AuthController extends BaseController
{
    protected $guard = 'api';

    /**
     * @SWG\Post(path="/authorizations",
     *   tags={"users"},
     *   summary="Loggin as User",
     *   description="Generates authentication token",
     *   operationId="store",
     *   produces={"application/xml", "application/json"},
     *   @SWG\Parameter(
     * 	   name="loginState",
     *     in="query",
     * 	   type="string",
     *     required=true,
     * 	   description="mynet user login state",
     * 	 ),
     *   @SWG\Response(response=400, description="Invalid id supplied"),
     *   @SWG\Response(response=404, description="not found")
     * )
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'loginState' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        //get user name
        $un = LoginState::getUserNick($request->loginState);
	    $credentials = $request->only(['USERNICK' => $un]);
        $user = User::where('USERNICK','=',$un)->first();
        if (!$token=\JWTAuth::fromUser($user)) {
            $this->response->errorUnauthorized(trans('auth.incorrect'));
        }
        $authorization = new Authorization($token);
        return $this->response->item($authorization, new AuthorizationTransformer())
            ->setStatusCode(201);
    }

    /**
    * @SWG\Put(path="/authorizations/current",
    *   tags={"users"},
    *   summary="Updates user token",
    *   description="This can only be done by the logged in user.",
    *   operationId="update",
    *   produces={"application/xml", "application/json"},
    *   @SWG\Response(response=400, description="Invalid token supplied"),
    *   @SWG\Response(response=404, description="PurchasedItem not found"),
    *   security={
    *       {"User_Bearer": {}}
    *   }
    * )
    */
    public function update()
    {
        $authorization = new Authorization(\Auth::refresh());

        return $this->response->item($authorization, new AuthorizationTransformer());
    }

    /**
    * @SWG\Delete(path="/authorizations/current",
    *   tags={"users"},
    *   summary="Deletes user token",
    *   description="This can only be done by the logged in user.",
    *   operationId="destroy",
    *   produces={"application/xml", "application/json"},
    *   @SWG\Response(response=400, description="Invalid token supplied"),
    *   @SWG\Response(response=404, description="token not found"),
    *   security={
    *       {"User_Bearer": {}}
    *   }
    * )
    */
    public function destroy()
    {
        \Auth::logout();

        return $this->response->noContent();
    }

}
