<?php

namespace App\Http\Controllers\Api\V1\Achievement;

use App\Http\Controllers\Api\V1\BaseController as BaseController;
use App\Models\Achievement\Achievement;
use App\Models\Achievement\Achievement_User;
use App\Transformers\AchievementTransformer;
use Swagger\Annotations as SWG;
use Illuminate\Http\Request;
use App\Utils;
use League\Fractal;
use App\Models\OverallScores;
use App\Models\LuckyBox;
use Illuminate\Support\Facades\Cache;
use Log;

class AchievementController extends BaseController {

    protected $guard = 'admin';

    /**
     * @SWG\Get(
     *   path="/achievements",
     *   tags={"achievements"},
     *   operationId="index",
     *   summary="list achievements",
     *   produces={"application/json"},
     *   @SWG\Parameter(
     * 	   name="DD_GAME_ID",
     *     in="query",
     * 	   type="string",
     *     required=false,
     * 	   description="game id",
     * 	 ),
     *     @SWG\Parameter(
     * 	   name="per_page",
     *     in="query",
     * 	   type="integer",
     *     required=false,
     * 	   description="per page for pagination",
     * 	 ),
     *   @SWG\Response(
     * 		status=200,
     * 	    description="success",
     * 		  @SWG\Schema(
     *          @SWG\Property(property="data", ref="#/definitions/Achievement")
     *      ),
     * 	 ),
     *   @SWG\Response(
     *     response="default",
     *     description="an ""unexpected"" error"
     *   )
     * )
     */
    public function index(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'DD_GAME_ID' => 'string',
            'per_page' => 'integer'
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        if ($request->has('DD_GAME_ID')) {
            $achievements = Achievement::where('DD_GAME_ID', $request->DD_GAME_ID)->orderBy('Position')->paginate($request->get('per_page') ?: 5);
        } else {
            $achievements = Achievement::orderBy('Position')->paginate($request->get('per_page') ?: 5);
        }
        return $this->response->paginator($achievements, new AchievementTransformer);
    }

    /**
    * @SWG\Get(
    *   path="/achievements/{code}",
    *   tags={"achievements"},
    *   operationId="show",
    *   summary="show achievement by code",
    *   produces={"application/json"},
    *   @SWG\Parameter(
    *       name="code",
    *       in="path",
    * 		required=true,
    * 		type="string",
    * 		description="achievement code",
    * 	),
    *   @SWG\Parameter(
    * 	   name="DD_GAME_ID",
    *      in="query",
    * 	   type="string",
    *      required=false,
    * 	   description="game id",
    * 	 ),
    *   @SWG\Response(
    * 		status=200,
    * 	    description="success",
    * 		  @SWG\Schema(
    *          @SWG\Property(property="data", ref="#/definitions/Achievement")
    *      ),
    * 	 ),
    *   @SWG\Response(
    *     response="default",
    *     description="an ""unexpected"" error"
    *   )
    * )
    */
    public function show($code, Request $request) {
        $validator = \Validator::make($request->all(), [
            'DD_GAME_ID' => 'string'
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        if ($request->has('DD_GAME_ID')) {
            $achievement = Achievement::where([
                ['DD_GAME_ID', $request->DD_GAME_ID],
                ['Code', $code]
            ])->get();
        } else {
            $achievement = Achievement::where('Code', $code)->get();
        }
        return $this->response->item($achievement, new AchievementTransformer);
    }



    /**
    * @SWG\Post(path="/admin/achievements",
    *   tags={"achievements"},
    *   summary="Creates an achievement",
    *   description="This can only be done by the logged in admin.",
    *   operationId="create",
    *   produces={"application/xml", "application/json"},
    *   @SWG\Parameter(
    *       name="Code",
    *       in="query",
    * 		required=true,
    * 		type="string",
    * 		description="achievement code",
    * 	),
    *   @SWG\Parameter(
    *       name="Group",
    *       in="query",
    * 		required=false,
    * 		type="string",
    * 		description="achievement group name",
    * 	),
    *   @SWG\Parameter(
    *       name="DD_GAME_ID",
    *       in="query",
    * 		required=true,
    * 		type="string",
    * 		description="game id",
    * 	),
    *   @SWG\Parameter(
    *       name="Title",
    *       in="query",
    * 		required=false,
    * 		type="string",
    * 		description="title",
    * 	),
    *   @SWG\Parameter(
    *       name="Description",
    *       in="query",
    * 		required=true,
    * 		type="string",
    * 		description="description",
    * 	),
    *   @SWG\Parameter(
    *       name="UniqueRecords",
    *       in="query",
    * 		required=false,
    * 		type="integer",
    * 		description="the achievement requires unique records per user",
    * 	),
    *   @SWG\Parameter(
    *       name="Interval",
    *       in="query",
    * 		required=false,
    * 		type="integer",
    * 		description="the days achievement needs to be complate",
    * 	),
    *   @SWG\Parameter(
    *       name="Uptime",
    *       in="query",
    * 		required=false,
    * 		type="integer",
    * 		description="check per given hour, this should be 24h",
    * 	),
    *   @SWG\Parameter(
    *       name="TargetPoint",
    *       in="query",
    * 		required=false,
    * 		type="integer",
    * 		description="Achievement target point to complete",
    * 	),
    *   @SWG\Parameter(
    *       name="UpdateOnesInUptime",
    *       in="query",
    * 		required=false,
    * 		type="boolean",
    * 		description="User can post only one time within given UpdateTime, usually in a day",
    * 	),
    *   @SWG\Parameter(
    *       name="Reward",
    *       in="query",
    * 		required=false,
    * 		type="string",
    * 		description="reward when the achievement completed",
    * 	),
    *   @SWG\Response(
    *       status=200,
    * 	    description="success",
    * 		  @SWG\Schema(
    *          @SWG\Property(property="data", ref="#/definitions/Achievement")
    *      ),
    *   ),
    *   @SWG\Response(
    * 	    status="default",
    * 		description="error",
    * 		@SWG\Schema(ref="#/definitions/Error"),
    *   ),
    *   security={
    *       {"Admin_Bearer": {}}
    *   }
    * )
    */
    public function store(Request $request) {
        $validator = \Validator::make($request->input(), [
            'Code' => 'required|string|max:100',
            'Group' => 'string|max:100',
            'DD_GAME_ID' => 'required|string',
            'Title' => 'string|max:255',
            'Description' => 'required|string|max:255',
            'UniqueRecords' => 'integer|digits_between:0,1',
            'Interval' => 'integer|digits_between:0,365',
            'Uptime' => 'integer',
            'TargetPoint' => 'integer|required',
         //   'UpdateOnesInUptime' => 'boolean', doesn't work with swagger
            'Reward' => 'string|max:255'
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        if (self::InvalidCodeForGame($request->Code, $request->DD_GAME_ID)) {
            throw new \Dingo\Api\Exception\StoreResourceFailedException('Code with DD_GAME_ID is exist, try with different Code name or Game Id.');
        }
        $attributes = $request->only('Code', 'DD_GAME_ID', 'Title', 'Description', 'UniqueRecords', 'TargetPoint');
        if ($request->has('Group')) {
            $attributes['Group'] = $request->Group;
        }
        if ($request->has('Interval')) {
            $attributes['Interval'] = $request->Interval;
        }
        if ($request->has('Reward')) {
            $attributes['Reward'] = $request->Reward;
        }
        if ($request->has('Uptime')) {
            $attributes['Uptime'] = $request->Uptime;
        }
        if ($request->has('UpdateOnesInUptime')) {
            $attributes['UpdateOnesInUptime'] = $request->UpdateOnesInUptime;
        }

        $attributes['Position'] = self::GetNextPosition($request->DD_GAME_ID);

        $achievement = Achievement::create($attributes);

        Cache::tags('ach')->flush();

       // $location = dingo_route('v1', 'achievements.show', $achievement->Code);
       return $this->response->item($achievement, new AchievementTransformer);
    }

    private static function InvalidCodeForGame($code, $gameId) {
        return Achievement::where([
            ['Code', $code],
            ['DD_GAME_ID', $gameId]
        ])->exists();
    }


    /**
     * @SWG\Post(path="/admin/achievements/moveup/{code}",
     *   tags={"achievements"},
     *   summary="Updates position of achievement",
     *   description="This can only be done by the logged in admin.",
     *   operationId="moveup",
     *   produces={"application/xml", "application/json"},
     *   @SWG\Parameter(
     *     name="code",
     *     in="path",
     *     description="achievement code",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     * 	   name="DD_GAME_ID",
     *     in="query",
     * 	   type="string",
     *     required=true,
     * 	   description="selected game id",
     * 	 ),
     *   @SWG\Response(response=400, description="Invalid id supplied"),
     *   @SWG\Response(response=404, description="not found"),
     *   security={
     *       {"Admin_Bearer": {}}
     *   }
     * )
     */
    public function moveup($code, Request $request) {
        $validator = \Validator::make($request->all(), [
            'DD_GAME_ID' => 'required|string'
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        $achievement = Achievement::where([
            ['Code', $code],
            ['DD_GAME_ID', $request->DD_GAME_ID]
        ])->firstOrFail();
        if(self::MoveUpPosition($achievement)) {
            Cache::tags('ach')->flush();
            return $this->response->item(Achievement::findOrFail($code), new AchievementTransformer);
        } else {
            $this->response->noContent();
        }
    }

    /**
     * @SWG\Post(path="/admin/achievements/movedown/{code}",
     *   tags={"achievements"},
     *   summary="Updates position of achievement",
     *   description="This can only be done by the logged in admin.",
     *   operationId="movedown",
     *   produces={"application/xml", "application/json"},
     *   @SWG\Parameter(
     *     name="code",
     *     in="path",
     *     description="achievement code",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     * 	   name="DD_GAME_ID",
     *     in="query",
     * 	   type="string",
     *     required=true,
     * 	   description="selected game id",
     * 	 ),
     *   @SWG\Response(response=400, description="Invalid id supplied"),
     *   @SWG\Response(response=404, description="not found"),
     *   security={
     *       {"Admin_Bearer": {}}
     *   }
     * )
     */
    public function movedown($code, Request $request) {
        $validator = \Validator::make($request->all(), [
            'DD_GAME_ID' => 'required|string'
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        $achievement = Achievement::where([
            ['Code', $code],
            ['DD_GAME_ID', $request->DD_GAME_ID]
        ])->firstOrFail();
        
        if(self::MoveDownPosition($achievement)) {
            Cache::tags('ach')->flush();
            return $this->response->item(Achievement::findOrFail($code), new AchievementTransformer);
        } else {
            $this->response->noContent();
        }
    }


    private static function GetNextPosition($gameId) {
        $maxPosition = 0;
        $count = Achievement::where('DD_GAME_ID', $gameId)->count();
        if ($count > 0) {
            $maxPosition = $count;
        }
        return $maxPosition;
    }

    private static function MoveUpPosition($achievement) {
        if ($achievement->Position > 0) {
            // Calculate the desired position
            $newPosition = $achievement->Position - 1;
            // Try to update the early positions
            $res = Achievement::where([
                ['DD_GAME_ID', $achievement->DD_GAME_ID],
                ['Position', $newPosition]
            ])->increment('Position');
            if ($res) {
                // Try to update the current position
                $res = Achievement::where([
                    ['DD_GAME_ID', $achievement->DD_GAME_ID],
                    ['Code', $achievement->Code]
                ])->update(['Position' => $newPosition]);

                if ($res) {
                    return TRUE;
                } else {
                    // Update current position failed, revert the previous statement
                    Achievement::where([
                        ['DD_GAME_ID', $achievement->DD_GAME_ID],
                        ['Position', '<=', $newPosition]
                    ])->decrement('Position');
                }
            }
        }
        return FALSE;
    }

    private static function MoveDownPosition($achievement) {
         $maxPosition = self::GetNextPosition($achievement->DD_GAME_ID) - 1;
         if ($maxPosition > 0 && $achievement->Position < $maxPosition) {
             // Calculate the desired position
             $newPosition = $achievement->Position + 1;
             // Try to update the early positions
             $res = Achievement::where([
                ['DD_GAME_ID', $achievement->DD_GAME_ID],
                ['Position', $newPosition]
            ])->decrement('Position');
            if ($res) {
                // Try to update the current position
                $res = Achievement::where([
                    ['DD_GAME_ID', $achievement->DD_GAME_ID],
                    ['Code', $achievement->Code]
                ])->update(['Position' => $newPosition]);

                if ($res) {
                    return TRUE;
                } else {
                    // Update current position failed, revert the previous statement
                    Achievement::where([
                        ['DD_GAME_ID', $achievement->DD_GAME_ID],
                        ['Position', '<=', $newPosition]
                    ])->increment('Position');
                }
            }
        }
        return FALSE;
    }



   /**
   * @SWG\Delete(path="/admin/achievements/{code}",
   *   tags={"achievements"},
   *   summary="Delete achievement",
   *   description="This can only be done by the logged in admin.",
   *   operationId="destroy",
   *   produces={"application/xml", "application/json"},
   *   @SWG\Parameter(
   *     name="code",
   *     in="path",
   *     description="Achievement code that needs to be deleted",
   *     required=true,
   *     type="string"
   *   ),
   *    @SWG\Parameter(
   * 	 name="DD_GAME_ID",
   *     in="query",
   * 	 type="string",
   *     required=true,
   * 	 description="selected game id",
   *   ),
   *   @SWG\Response(response=400, description="Invalid achievement code supplied"),
   *   @SWG\Response(response=404, description="achievement not found"),
   *   security={
   *       {"Admin_Bearer": {}}
   *   }
   * )
   */
    public function destroy($code, Request $request) {
        $validator = \Validator::make($request->all(), [
            'DD_GAME_ID' => 'required|string'
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        $achievement = Achievement::where([
            ['Code', $code],
            ['DD_GAME_ID', $request->DD_GAME_ID]
        ])->firstOrFail();
        // Reorder the next positions
        $position = $achievement->Position;
        if ($achievement->delete()) {
            Achievement::where("Position", ">", $position)->decrement('Position');
        }
        Cache::tags('ach')->flush();
    }

    public function groupProgressForUser($group, Request $request) {
        Utils::CheckWebsericeSecurity();
        $validator = \Validator::make($request->all(), [
            'DD_GAME_ID' => 'required|string',
            'Point' => 'required|integer',
            'Usernick' => 'required|string'
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        $gameId = $request->DD_GAME_ID;
        $point = $request->Point;
        $usernick = $request->Usernick;
        return self::Progress($gameId, $usernick, $group, $point);
    }


     /**
     * @SWG\Post(path="/achievements/groups/{group}",
     *   tags={"achievements"},
     *   summary="Updates user's achievement progress",
     *   description="This can only be done by the logged in user.",
     *   operationId="groupProgress",
     *   produces={"application/xml", "application/json"},
     *   @SWG\Parameter(
     *     name="group",
     *     in="path",
     *     description="achievement group code",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     * 	   name="DD_GAME_ID",
     *     in="query",
     * 	   type="string",
     *     required=true,
     * 	   description="selected game id",
     * 	 ),
     *   @SWG\Parameter(
     * 	   name="Point",
     *     in="query",
     * 	   type="integer",
     *     required=true,
     * 	   description="point value to reach TargetPoint",
     * 	 ),
     *   @SWG\Response(response=400, description="Invalid id supplied"),
     *   @SWG\Response(response=404, description="not found"),
     *   security={
     *       {"User_Bearer": {}}
     *   }
     * )
     */
    
    public function groupProgress($group, Request $request) {

        $validator = \Validator::make($request->all(), [
            'DD_GAME_ID' => 'required|string',
            'Point' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        $user = $this->user();
        $gameId = $request->DD_GAME_ID;
        $point = $request->Point;
        return self::Progress($gameId, $user->USERNICK, $group, $point);
    }


    private static function Progress($gameId, $usernick, $group, $point) {
        // force to load from db when user's achievements are requested.
        Cache::tags(['ach', 'ua'])->forget('ua_'.$usernick);

        $cache_tags = ['ach'];
        $cache_key = 'acg_' . $gameId . '_' . $group;

        if (!$achievements = Cache::tags($cache_tags)->get($cache_key)) {
            $achievements = Achievement::where([
                ['Group', $group],
                ['DD_GAME_ID', $gameId],
                ['IsActive', 1]
            ])->orderBy('Interval', 'asc')->get();
            if (!$achievements) {
                abort(403);
            }
            Cache::tags($cache_tags)->put($cache_key, $achievements, env('CACHE_DEFAULT_EXPIRE', 120));
        }

        
        
        $point = max(0, $point);

        $canBeClaimed = array();

        // eksik achievement varsa kullanıcı icin yarat
        foreach($achievements as $ach) {
            $ua = Achievement_User::where([
                ['USERNICK', $usernick],
                ['IdAchievement', $ach->id]
            ])->first();
            
            $updatedUserAchevement = self::UpdateAchievementPoint($ua, $ach, $point, $usernick);

            // currentpoint target'i gecmisse hakedisini verebilirsin
            if (!is_null($updatedUserAchevement) && $updatedUserAchevement->CurrentPoint >= $ach->TargetPoint
                && !$updatedUserAchevement->RewardClaimed && !is_null($ach->Reward)
                && Utils::GetCurrentDateTime() < Utils::GetDateTime($updatedUserAchevement->ExpiredAt)) {
                    $canBeClaimed[] = array(
                        "Code" => $ach->Code,
                        "Group" => $ach->Group,
                        "DD_GAME_ID" => $ach->DD_GAME_ID,
                        "Title" => $ach->Title,
                        "Description" => $ach->Description,
                        "Position" => $ach->Position,
                        "Reward" => $ach->Reward,
                        "CurrentPoint" => $updatedUserAchevement->CurrentPoint,
                        "TargetPoint" => $ach->TargetPoint
                    );
            }

        }

        return response()->json($canBeClaimed);
    }


    /**
     *  update achievement score
     */
    private static function UpdateAchievementPoint($ua, $ach, $point, $username) {
        if ($ach->Interval > 0) {
            if (is_null($ua)) {
                $ua = new Achievement_User();
                $ua->IdAchievement = $ach->id;
                $ua->USERNICK = $username;

                $ua->ExpiredAt = Utils::GetDateTime(sprintf("%s 00:00:00", date("Y-m-d")))->modify($ach->Interval.' day')->format("Y-m-d H:i:s");
                $ua->UpdatedAt = Utils::GetDateTime(sprintf("%s 00:00:00", date("Y-m-d")))->format("Y-m-d H:i:s");
                $ua->CurrentPoint = min($point, $ach->TargetPoint);
                $ua->save();
            } else {
                // time interval restriction if exist
                // Do we need update ?
                $now = Utils::GetCurrentDateTime();
                //test $now = Utils::GetDateTime("2018-06-07 14:07:00");
                //test $today = "2018-06-07 00:00:00";
                $updatedAt = Utils::GetDateTime($ua->UpdatedAt);
                $differenceThanLastUpdated = Utils::GetDiffHours($now, $updatedAt);
                $expiredAt = Utils::GetDateTime($ua->ExpiredAt);
                $differenceThanExpired = Utils::GetDiffHours($now, $expiredAt);

                if ($now >= $expiredAt || $differenceThanLastUpdated >= $ach->Uptime * 2) {
                    // differenceThanExpired > 24 son gunu kullanamamis, gecerli sure sona ermis.resetle.
                    // differenceThanLastUpdated > 48 - ikinci gune girmis, basarisiz olmus

                    // Check to see if the achievement requires unique records per user
                    if ($ach->UniqueRecords == 1) {
                        return $ua;
                    }

                    // resetle
                    $today = sprintf("%s 00:00:00", date("Y-m-d"));
                    $ua->ExpiredAt = Utils::GetDateTime($today)->modify($ach->Interval.' day')->format("Y-m-d H:i:s");
                    $ua->UpdatedAt = Utils::GetDateTime($today)->format("Y-m-d H:i:s");
                    $ua->CurrentPoint = min($point, $ach->TargetPoint);
                    $ua->RewardClaimed = false;
                    $ua->save();

                } else if ($ach->UpdateOnesInUptime && $differenceThanLastUpdated <= $ach->Uptime){
                    // ayni gun update edilmis, birsey yapmiyoruz.
                } else {
                    // sikinti yok, current point'i guncelle
                    // Bir gun gecmis, kurala uygun, update et
                    if ($point > 0 && $ua->CurrentPoint < $ach->TargetPoint) {
                        $newPoint = $ua->CurrentPoint + $point;
                        $ua->CurrentPoint = min($newPoint, $ach->TargetPoint);
                        $today = sprintf("%s 00:00:00", date("Y-m-d"));
                        $ua->UpdatedAt = Utils::GetDateTime($today)->format("Y-m-d H:i:s");
                        $ua->save();
                    }
                }
                // end time interval restriction if exist
            }
            
        } else {
            // TODO
        }

        return $ua;
    }



     /**
     * @SWG\Get(path="/user/achievements",
     *   tags={"achievements"},
     *   summary="Gets user's achievements",
     *   description="This can only be done by the logged in user.",
     *   operationId="userIndex",
     *   produces={"application/xml", "application/json"},
     *   @SWG\Parameter(
     * 	   name="DD_GAME_ID",
     *     in="query",
     * 	   type="string",
     *     required=true,
     * 	   description="selected game id",
     * 	 ),
     *   @SWG\Response(response=400, description="Invalid id supplied"),
     *   @SWG\Response(response=404, description="not found"),
     *   security={
     *       {"User_Bearer": {}}
     *   }
     * )
     */
    public function userIndex(Request $request) {
        $validator = \Validator::make($request->all(), [
            'DD_GAME_ID' => 'string|required'
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }

        $user = $this->user();
        
        $res = $this->getUserAchievements($user->USERNICK, $request->DD_GAME_ID);
        return $this->response->collection($res, new AchievementTransformer)
          ->setMeta(['count' => $res->count()]);
    }

    public function getUserAchievements($usernick, $gameid) {

        $cache_tags = ['ach', 'ua'];
        $cache_key = 'ua_'.$usernick;

        if (!$results = Cache::tags($cache_tags)->get($cache_key)) {
            $results = array();
            $achievements = Achievement::where([
                ['DD_GAME_ID', $gameid],
                ['IsActive', 1]
            ])->orderBy('Interval', 'asc')->get();
    
            if (!$achievements) {
                abort(403);
            }
    
            foreach($achievements as $ach) {
    
                $ua = Achievement_User::where([
                    ['USERNICK', $usernick],
                    ['IdAchievement', $ach->id]
                ])->first();
    
    
                if (is_null($ua)) {
                //    $item["CurrentPoint"] = 0;
                    $ach->CurrentPoint = 0;
                } else {
                    if ($ach->Interval > 0) { // time interval restriction if exist
                        // Do we need update ?
                        $now = Utils::GetCurrentDateTime();
                        //test $now = Utils::GetDateTime("2018-06-07 14:07:00");
                        //test $today = "2018-06-07 00:00:00";
                        $updatedAt = Utils::GetDateTime($ua->UpdatedAt);
                        $differenceThanLastUpdated = Utils::GetDiffHours($now, $updatedAt);
                        $expiredAt = Utils::GetDateTime($ua->ExpiredAt);
                        $differenceThanExpired = Utils::GetDiffHours($now, $expiredAt);
    
                        if ($now >= $expiredAt || $differenceThanLastUpdated >= $ach->Uptime * 2) {
                            // differenceThanExpired > 24 son gunu kullanamamis, gecerli sure sona ermis.resetle.
                            // differenceThanLastUpdated > 48 - ikinci gune girmis, basarisiz olmus
                            // basarisiz ama tekrar yapilabilir. sifir gibi gosterecegiz.
    
                            // Check to see if the achievement requires unique records per user
                            if ($ach->UniqueRecords == 1) {
                                continue;
                            }
                            $ua->CurrentPoint = 0;
                            $ua->RewardClaimed = false; 
                        } else {
                            if ($ach->RewardClaimed) {
                                continue;
                            }
                        }
                    } // end time interval restriction
                  $ach->CurrentPoint = $ua->CurrentPoint;
                  $ach->RewardClaimed = $ua->RewardClaimed;
                }
                $results[] = $ach;
            }
            Cache::tags($cache_tags)->put($cache_key, $results, env('CACHE_DEFAULT_EXPIRE', 120));
        }

        return collect($results);
    }


    /**
     * @SWG\Get(path="/user/achievements/consume",
     *   tags={"achievements"},
     *   summary="Consume user's achievement reward",
     *   description="This can only be done by the logged in user.",
     *   operationId="consumeReward",
     *   produces={"application/xml", "application/json"},
     *   @SWG\Parameter(
     * 	   name="DD_GAME_ID",
     *     in="query",
     * 	   type="string",
     *     required=true,
     * 	   description="selected game id",
     * 	 ),
     *   @SWG\Parameter(
     * 	   name="Code",
     *     in="query",
     * 	   type="string",
     *     required=true,
     * 	   description="achievement code id",
     * 	 ),
     *   @SWG\Response(response=400, description="Invalid id supplied"),
     *   @SWG\Response(response=404, description="not found"),
     *   security={
     *       {"User_Bearer": {}}
     *   }
     * )
     */
    public function consumeReward(Request $request) {
        $validator = \Validator::make($request->all(), [
            'DD_GAME_ID' => 'string|required',
            'Code' => 'string|required'
        ]);
        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        $user = $this->user();

        Cache::tags(['ach', 'ua'])->forget('ua_'.$user->USERNICK);

        $ach = Achievement::find($request->Code);
        if (is_null($ach)) {
            Log::critical("ach consume - Achievement not found");
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('Achievement not found');
        }
        if (!$ach->IsActive) {
            return $this->response->noContent();
        }
        $ua = Achievement_User::where([
            ['USERNICK', $user->USERNICK],
            ['IdAchievement', $ach->id]
        ])->first();
        if (is_null($ach)) {
            Log::critical("ach consume - User Achievement not found");
            throw new \Symfony\Component\HttpKernel\Exception\NotFoundHttpException('User Achievement not found');
        }

        if ($ach->Interval > 0) { // time interval restriction if exist
            $now = Utils::GetCurrentDateTime();
            //test $now = Utils::GetDateTime("2018-05-12 14:07:00");
            //test  $today = "2018-05-12 00:00:00";
            $updatedAt = Utils::GetDateTime($ua->UpdatedAt);
            $differenceThanLastUpdated = Utils::GetDiffHours($now, $updatedAt);
            $expiredAt = Utils::GetDateTime($ua->ExpiredAt);
            $differenceThanExpired = Utils::GetDiffHours($now, $expiredAt);

            if ($now >= $expiredAt || $differenceThanLastUpdated >= $ach->Uptime * 2) {
                // differenceThanExpired > 24 son gunu kullanamamis, gecerli sure sona ermis.resetle.
                // differenceThanLastUpdated > 48 - ikinci gune girmis, basarisiz olmus
                // basarisiz ama tekrar yapilabilir. sifir gibi gosterecegiz.
                Log::debug("ach consume - expired 1 ach id:" . $ach->id . " ua id:" . $ua->id . " user: " . $user->USERNICK);
                return $this->response->noContent();
            } else {
                if ($ach->RewardClaimed) {
                    Log::debug("ach consume - expired 2 ach id:" . $ach->id . " ua id:" . $ua->id . " user: " . $user->USERNICK);
                    return $this->response->noContent();
                }
            }
        }
        // currentpoint target'i gecmisse hakedisini verebilirsin
        if ($ua->CurrentPoint >= $ach->TargetPoint && !$ua->RewardClaimed && !is_null($ach->Reward)) {
            // puanlar icin burada yapıyoruz until 2038
            if (strpos($ach->Reward, "26_puan_") === 0) {
                $addToScore = (int)(str_replace("26_puan_", "", $ach->Reward));
                if ($addToScore >= 100 && $addToScore <= 15000) { // to be safe
                    $score = OverallScores::where([
                        ['DD_GAME_ID', $ach->DD_GAME_ID],
                        ['USERNICK', $user->USERNICK]])
                        ->increment('TOTAL_SCORE', $addToScore);

                }
                $ua->RewardClaimed = true;
                $ua->save();
                if ($addToScore > 1500) {
                    Log::info("ach consume success point - ach id:" . $ach->id . " ua id:" . $ua->id . " user: " . $user->USERNICK);
                }
            } else if (strpos($ach->Reward, "26_sanskutusu_") === 0) {
                $count = (int)(str_replace("26_sanskutusu_", "", $ach->Reward));
                if ($count >= 1 && $count <= 10) { // to be safe
                    $dateToday =  Utils::GetCurrentDateTimeFormat("Y-m-d");
                    for ($i = 0; $i < $count; $i++) {
                        $luckybox = new LuckyBox();
                        $luckybox->usernick = $user->USERNICK;
                        $luckybox->create_date = Utils::GetCurrentDateTimeFormat();
                        $luckybox->paid = 1;
                        $luckybox->market_id = "ach_" . $ach->id . "_" . $dateToday . "_" . $i;
                        $luckybox->save();
                    }
                    $ua->RewardClaimed = true;
                    $ua->save();
                    if (Cache::has("Sans_Kutusu_".$user->USERNICK)) {
                        Cache::forget("Sans_Kutusu_".$user->USERNICK);
                    }
                }
                Log::info("ach consume success sanskutusu - ach id:" . $ach->id . " ua id:" . $ua->id . " user: " . $user->USERNICK);
            }
            $ach->RewardClaimed = $ua->RewardClaimed;
            $ach->CurrentPoint = $ua->CurrentPoint;
            return $this->response->item($ach, new AchievementTransformer);
        }

        return $this->response->noContent();
        
    }

}
