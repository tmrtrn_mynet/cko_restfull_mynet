<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Models\AdminAccount;
use Illuminate\Http\Request;
use App\Models\Authorization;
use App\Transformers\AdminTransformer;
use App\Http\Controllers\Api\V1\BaseController as BaseController;

class AdminController extends BaseController
{
    protected $guard = 'admin';
    /**
    * @SWG\Get(path="/admins",
    *   tags={"admin"},
    *   summary="Updates admin password",
    *   description="This can only be done by the logged in admin.",
    *   operationId="index",
    *   produces={"application/xml", "application/json"},
    *   @SWG\Response(response=400, description="Invalid token supplied"),
    *   @SWG\Response(response=404, description="not found"),
    *   security={
    *       {"Admin_Bearer": {}}
    *   }
    * )
    */
    public function index(AdminAccount $admin)
    {
        $admins = AdminAccount::paginate();

        return $this->response->paginator($admins, new AdminTransformer());
    }

   /**
    * @SWG\Put(path="/admin/password",
    *   tags={"admin"},
    *   summary="Updates admin password",
    *   description="This can only be done by the logged in admin.",
    *   operationId="editPassword",
    *   produces={"application/xml", "application/json"},
    *   @SWG\Parameter(
    *       name="old_password",
    *       in="query",
    *       description="current password",
    *       required=true,
    *       type="string"
    *   ),
    *   @SWG\Parameter(
    * 	    name="password",
    *       in="query",
    *       type="string",
    *       required=true,
    * 		description="new password",
    * 	),
    *   @SWG\Parameter(
    * 	    name="password_confirmation",
    *       in="query",
    *       type="string",
    *       required=true,
    * 		description="new password again",
    * 	),
    *   @SWG\Response(response=400, description="Invalid token supplied"),
    *   @SWG\Response(response=404, description="not found"),
    *   security={
    *       {"Admin_Bearer": {}}
    *   }
    * )
    */
    public function editPassword(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'old_password' => 'required',
            'password' => 'required|confirmed|different:old_password',
            'password_confirmation' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return $this->errorBadRequest($validator);
        }
        $admin = $this->guard()->user();

        $auth = $this->guard()->once([
            'username' => $admin->username,
            'password' => $request->get('old_password'),
        ]);

        if (!$auth) {
            return $this->response->errorUnauthorized();
        }

        $password = app('hash')->make($request->get('password'));
        $admin->update(['password' => $password]);

        return $this->response->noContent();
    }

    /**
     * @SWG\Get(path="/admins/{username}",
     *   tags={"admin"},
     *   summary="gets selected admin",
     *   description="This can only be done by the logged in admin.",
     *   operationId="show",
     *   produces={"application/xml", "application/json"},
     *   @SWG\Parameter(
     *     name="username",
     *     in="path",
     *     description="admin username",
     *     required=true,
     *     type="string"
     *   )
     *   @SWG\Response(response=400, description="Invalid token supplied"),
     *   @SWG\Response(response=404, description="not found"),
     *   security={
     *       {"Admin_Bearer": {}}
     *   }
    * )
 */
    public function show($username)
    {
        $admin = AdminAccount::where('username', $username)->firstOrFail();
        return $this->response->item($admin, new AdminTransformer());
    }

    /**
    * @SWG\Get(path="/admin",
    *   tags={"admin"},
    *   summary="Shows active admin",
    *   description="hows active admin",
    *   operationId="adminShow",
    *   produces={"application/xml", "application/json"},
    *   @SWG\Response(response=400, description="Invalid id supplied"),
    *   @SWG\Response(response=404, description="not found"),
    *   security={
    *       {"Admin_Bearer": {}}
    *   }
    * )
    */
    public function adminShow()
    {
        return $this->response->item($this->guard()->user(), new AdminTransformer());
    }
}
