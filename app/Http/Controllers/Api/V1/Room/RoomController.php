<?php

namespace App\Http\Controllers\Api\V1\Room;

use App\Http\Controllers\Api\V1\BaseController as BaseController;
use App\RoomSold;
use App\RoomMynet;
use App\Room;
use App\Utils;
use App\Models\Room\RoomCustomData;
use Illuminate\Http\Request;
use Swagger\Annotations as SWG;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class RoomController extends BaseController
{
/**
 * @SWG\Get(
 *   path="/games/{game}/rooms",
 *   tags={"rooms"},
 *   operationId="showAllRooms",
 *   summary="list rooms",
 *   produces={"application/json"},
 * 	 @SWG\Parameter(
 * 	   name="game",
 * 		 in="path",
 * 		 required=true,
 * 		 type="string",
 * 		 description="DD_GAME_ID",
 * 	 ),
 *   @SWG\Response(
 * 		status=200,
 * 	  description="success",
 * 		@SWG\Schema(
 *          @SWG\Property(property="data", ref="#/definitions/RoomSold"),
 *          @SWG\Property(property="data", ref="#/definitions/RoomMynet")
 *      ),
 * 	 ),
 *   @SWG\Response(
 *     response="default",
 *     description="an ""unexpected"" error"
 *   ),
 *    security={
 *       {"Admin_Bearer": {}}
 *   }
 * )
 */
  public function showAllRooms($game, Request $request)
  {
   // DB::connection()->enableQueryLog();

    $mynet = RoomMynet::with('roomTypeDetails')->where('DD_GAME_ID', $game)->get();
    $vip = RoomSold::with('roomTypeDetails')->where('DD_GAME_ID', $game)->get();
      
     // $query = DB::getQueryLog();
     // var_dump($query);
      return response()->json(['mynet' => $mynet, 'vip' => $vip], 200, [], JSON_UNESCAPED_UNICODE);
  }


  /**
	 * @SWG\Get(
	 * 		path="/games/{game}/rooms/{roomType}",
	 * 		tags={"rooms"},
	 * 		operationId="showRooms",
	 * 		summary="Fetch rooms as mynet or vip",
	 * 		@SWG\Parameter(
	 * 			name="game",
	 * 			in="path",
	 * 			required=true,
	 * 			type="string",
	 * 			description="DD_GAME_ID",
	 * 		),
   *    @SWG\Parameter(
   * 	   name="roomType",
   * 		 in="path",
   * 		 required=true,
   * 		 type="string",
   * 		 description="vip or mynet",
   * 	 ),
   *    @SWG\Parameter(
   * 	   name="type",
   *     in="query",
   * 		 type="string",
   * 		 description="ex:type=0,1 SOLD_ROOMS=>{ 1-Required SOLD_USER_LIST registiration, 2-General } MYNET_ROOMS=>{ 0-General}",
   *     required=false,
   * 	 ),
   *   @SWG\Parameter(
   * 	   name="visibility",
   *     in="query",
   * 		 type="boolean",
   *     required=false,
   * 		 description="visible to everyone or private room",
   * 	 ),
   *   @SWG\Parameter(
   * 	   name="per page",
   *     in="query",
   * 		 type="integer",
   *     required=false,
   * 		 description="returns pagination result count per page",
   * 	 ),
   *   @SWG\Parameter(
   * 	   name="page",
   *     in="query",
   * 		 type="integer",
   *     required=false,
   * 		 description="returns active page results",
   * 	 ),
   *   @SWG\Parameter(
   * 	   name="keywords",
   *     in="query",
   * 		 type="string",
   *     required=false,
   * 		 description="returns search results over ROOMNAMETR",
   * 	 ),
   *   @SWG\Parameter(
   * 	   name="sort",
   *     in="query",
   * 		 type="string",
   *     required=false,
   * 		 description="updated_transaction - expire - name",
   *   ),
   *   @SWG\Parameter(
   * 	   name="order",
   *     in="query",
   * 		 type="string",
   *     required=false,
   * 		 description="asc - desc",
   *   ),
	 * 		@SWG\Response(
	 * 			status=200,
	 * 			description="success",
	 * 			@SWG\Schema(
   *              @SWG\Property(property="data", ref="#/definitions/Room")
   *          ),
	 * 		),
	 * 		@SWG\Response(
	 * 			status="default",
	 * 			description="error",
	 * 			@SWG\Schema(ref="#/definitions/Error"),
	 * 		),
   *     security={
   *       {"Admin_Bearer": {}}
   *     }
	 * 	)
	 *
	 */
  public function showRooms($game, $roomType, Request $request) {
    DB::connection()->enableQueryLog();
    $pp = 15;
    if ($request->has('perPage')) {
      $pp = $request->perPage;
    }
    if (strcmp($roomType, "vip") === 0) {
      $query = $this->showRoomsByType($request, RoomSold::class);
      $query->whereNotNull('PAID_EXPIRE')->whereDate('PAID_EXPIRE', '>', Utils::GetCurrentDateTimeFormat('Y-m-d H:i:s'));
    } else if (strcmp($roomType, "mynet") === 0) {
      $query = $this->showRoomsByType($request, RoomMynet::class);
    }
    $allowedSort    = array('name','updated_transaction', 'expire');
    $sort       = in_array(Input::get('sort'), $allowedSort) ? Input::get('sort') : 'name';
    $order      = Input::get('order') === 'desc' ? 'desc' : 'asc';
    if ($sort == 'updated_transaction') {
      $query->whereHas($order == 'desc' ? 'latestTransaction' : 'firstTransaction', function($query){
        $query->where('PRODUCT', 'LIKE', '%_oda_%');
        $query->where('RESULT', '=', 'ok');
      })/*
      ->with(['latestTransaction' => function($query){
        
       }]) */
      //https://zaengle.com/blog/sorting-parent-models-by-a-child-relationship
    //     ->join('TRANSACTION_LOG', 'TRANSACTION_LOG.ROOM_ID', '=', 'sold_rooms.ROOMID')
     //   ->select('sold_rooms.ROOMID', DB::raw('(select max(ADD_DATE) from TRANSACTION_LOG)'))       // just to avoid fetching anything from joined table
      //  ->groupBy('TRANSACTION_LOG.ADD_DATE', 'sold_rooms.ROOMID')
       
      //   ->orderBy('TRANSACTION_LOG.ADD_DATE', 'DESC')
     //    ->groupBy('sold_rooms.ROOMID')
     //   $query->with('transaction')
     //   ->select('sold_rooms.*', \DB::raw('(SELECT ADD_DATE FROM TRANSACTION_LOG WHERE sold_rooms.ROOMID = TRANSACTION_LOG.ROOM_ID ) as sort'))
       // ->orderBy('sort');
      ; 

    } else if($sort == 'expire') {
      $query->orderBy('PAID_EXPIRE', $order);
    } else {
      $query->orderBy('ROOMNAME', $order);
    }
    
    

    $query = $query
        ->with('roomTypeDetails')
        ->where('DD_GAME_ID', $game);
    if ($request->has('keywords')) {
      $query = $query -> search($request->keywords);
    }
    $rooms = $query->paginate($pp);
    $cvbn = DB::getQueryLog();
   // var_dump($cvbn);

    return response()->json($rooms, 200, [], JSON_UNESCAPED_UNICODE);
  }

  function showRoomsByType($request, $model) {
    $value = $model::whereHas('roomTypeDetails', function($query) use ($request) {
      if ($request->has('type')) {
        $roomType = explode(',',$request->type);
        $query->whereIn("TYPE", $roomType);
      }
      if ($request->has('visibility')) {
        $visibility = explode(',',$request->visibility);
        $query->whereIn("visibility", $visibility);
      }
    });
    return $value;
  }


  /**
	 * @SWG\Get(
	 * 		path="/rooms/{roomId}",
	 * 		tags={"rooms"},
	 * 		operationId="showRoomById",
	 * 		summary="Get room details",
	 * 		@SWG\Parameter(
	 * 			name="roomId",
	 * 			in="path",
	 * 			required=true,
	 * 			type="integer",
	 * 			description="room id",
	 * 		),
	 * 		@SWG\Response(
	 * 			status=200,
	 * 			description="success",
	 * 			@SWG\Schema(
   *              @SWG\Property(property="data", ref="#/definitions/RoomSold"),
   *              @SWG\Property(property="data", ref="#/definitions/RoomMynet"),
   *          ),
	 * 		),
	 * 		@SWG\Response(
	 * 			status="default",
	 * 			description="error",
	 * 			@SWG\Schema(ref="#/definitions/Error"),
	 * 		),
   *    security={
   *       {"Admin_Bearer": {}}
   *     }
	 * 	)
	 *
	 */
  public function showRoomById($roomId) {
    $room = $this->getRoomById($roomId);
    return response()->json($room, 200, [], JSON_UNESCAPED_UNICODE);
  }

  private function getRoomById($id) {
    $roomId = intval($id);
    if ($roomId > 5000) {
      // we assume a room id greater than 500 is sold room, we will check sold_rooms table first in order to reduce process.
      $room = RoomSold::with('roomTypeDetails')->findOrFail($roomId)
        ->makeVisible(RoomSold::$visibleDetails);
      if(is_null($room)) {
        $room = RoomMynet::findOrFail($roomId)
          ->makeVisible(RoomMynet::$visibleDetails);
      }
    } else {
      // this might be a mynet room
      $room = RoomMynet::findOrFail($roomId)
        ->makeVisible(RoomMynet::$visibleDetails);
      if(is_null($room)) {
        $room = RoomSold::findOrFail($roomId)
          ->makeVisible(RoomSold::$visibleDetails);
      }
    }
    return $room;
  }

/**
 * @SWG\Get(
 * 		path="/rooms/{roomId}/customData",
 * 		tags={"rooms"},
 * 		operationId="getCustomData",
 * 		summary="Get Custom Data Value By Key",
 *    @SWG\Parameter(
 * 			name="roomId",
 * 			in="path",
 * 			required=true,
 * 			type="integer",
 * 			description="room id",
 * 		),
 *    @SWG\Parameter(
* 	   name="keys",
*      in="query",
* 		 type="string",
*      required=true,
* 		 description="Data Keys which requested ex: SelectedIstaka,BlockedGuests",
* 	 ),
 * 		@SWG\Response(
 * 			status=200,
 * 			description="success",
 * 			@SWG\Schema(
 *              @SWG\Property(property="data", ref="#/definitions/RoomCustomData"),
 *          ),
 * 		),
 * 		@SWG\Response(
 * 			status="default",
 * 			description="error",
 * 			@SWG\Schema(ref="#/definitions/Error"),
 * 		),
 *     security={
 *       {"User_Bearer": {}}
 *     }
 * 	)
 *
 */
  public function getCustomData($roomId, Request $request) {
    $success = false;
    $message = "";
    try {
      $validator = \Validator::make($request->all(), [
          'keys' => 'required|string'
      ]);
      if ($validator->fails()) {
          return $this->errorBadRequest($validator);
      }

      $user = $this->user();

      $room = $this->getRoomById($roomId);
      if ($room == null) {
        return response()->json([
          'success' => $success,
          'status' => 500,
          'message' => 'room not found',
        ]);
      }
      if (strcmp($user->USERNICK, $room->OWNER) !== 0) {
        return response()->json([
          'success' => $success,
          'status' => 500,
          'message' => 'the player is not owner the room',
        ]);
      }
      $success = true;
      $keysArr = explode(',',$request->keys);
      $message = RoomCustomData::where('ROOMID', $roomId)->whereIn("DataKey", $keysArr)->get();

    } catch(\Illuminate\Database\QueryException $ex) {
      $success = false;
      $message = "ex " . $ex->getMessage();
    }
    return response()->json([
      'success' => $success,
      'status' => $success ? 200 : 500,
      'message' => $message,
    ]);
  }

  /**
 * @SWG\Post(path="/rooms/{roomId}/customData",
 *   tags={"rooms"},
 *   summary="Sets custom data for selected room with key-value",
 *   description="Keys must be unique",
 *   operationId="setCustomData",
 *   produces={"application/xml", "application/json"},
 *   @SWG\Parameter(
 * 			name="roomId",
 * 			in="path",
 * 			required=true,
 * 			type="integer",
 * 			description="room id",
 * 		),
*    @SWG\Parameter(
* 	   name="keys",
*      in="query",
* 		 type="string",
*      required=true,
* 		 description="Data keys, Should be CamelCase please ex: SelectedIstaka,BlockedGuests",
* 	 ),
*    @SWG\Parameter(
* 	   name="values",
*      in="query",
* 		 type="string",
*      required=true,
* 		 description="Data Values, ex: vipistaka, true",
* 	 ),
*    @SWG\Parameter(
* 	   name="skus",
*      in="query",
* 		 type="string",
*      required=false,
* 		 description="Skulist, ex: vipistaka,istakasb",
* 	 ),
*    security={
*       {"User_Bearer": {}}
*    },
*   @SWG\Response(response=400, description="Invalid input supplied"),
*   @SWG\Response(response=404, description="RoomCustomData not found")
* )
*/
  public function setCustomData($roomId, Request $request) {
    $success = false;
    $message = "";
    $validator = \Validator::make($request->all(), [
        'keys' => 'required|string|max:1000',
        'values' => 'required'
    ]);
    if ($validator->fails()) {
        return $this->errorBadRequest($validator);
    }

    try {
      $user = $this->user();

      $room = $this->getRoomById($roomId);
      if ($room == null) {
        return response()->json([
          'success' => $success,
          'status' => 500,
          'message' => 'room not found',
        ]);
      }

      if (strcmp($user->USERNICK, $room->OWNER) !== 0) {
        return response()->json([
          'success' => $success,
          'status' => 500,
          'message' => 'the player is not owner the room',
        ]);
      }

      $keysArr = explode(',',$request->keys);
      $valuesArr = explode(',',$request->values);
      $skuArr = [];
      if ($request->has('skus')) {
        $skuArr = explode(',',$request->skus);
      }
      for ($i = 0; $i < count($keysArr); $i++) {
        $customData = RoomCustomData::firstOrNew(
          ['ROOMID' => $roomId, 'DataKey' => $keysArr[$i]]
        );
        $customData->DataValue = $valuesArr[$i];
        if ($i < count($skuArr) && isset($skuArr[$i]) && !empty($skuArr[$i])) {
          $customData->ProductSku =  $skuArr[$i];
        } else {
          $customData->ProductSku = NULL;
        }
        $customData->save();
      }
      $success = true;

    } catch(\Illuminate\Database\QueryException $ex) {
      $success = false;
      $message = "ex " . $ex->getMessage();
    }
    return response()->json([
      'success' => $success,
      'status' => $success ? 200 : 500,
      'message' => $message,
    ]);

  }



  /**
 * @SWG\Get(
 * 		path="/rooms/{roomId}/validpurchase",
 * 		tags={"rooms"},
 * 		operationId="validpurchase",
 * 		summary="Gets active purchased items with belongsTo",
 * 		@SWG\Parameter(
 * 			name="roomId",
 * 			in="path",
 * 			required=true,
 * 			type="string",
 * 			description="belongsTO - roomid",
 * 		),
 *    @SWG\Parameter(
* 	   name="sku",
*      in="query",
* 		 type="string",
*      required=false,
* 		 description="sku list ex:istakasb,istakask",
* 	 ),
 * 		@SWG\Response(
 * 			status=200,
 * 			description="success",
 * 			@SWG\Schema(
 *        @SWG\Property(property="data", ref="#/definitions/RoomProduct")
 *      ),
 * 		),
 * 		@SWG\Response(
 * 			status="default",
 * 			description="error",
 * 			@SWG\Schema(ref="#/definitions/Error"),
 * 		),
 *     security={
 *       {"User_Bearer": {}}
 *    },
 * 	)
 *
 */
  public function validpurchase($roomId, Request $request) {
    $user = $this->user();

    $room = $this->getRoomById($roomId);
    if ($room == null) {
      return response()->json([
        'success' => false,
        'status' => 404,
        'message' => 'room not found',
      ]);
    }
    if (strcmp($user->USERNICK, $room->OWNER) !== 0) {
      return response()->json([
        'success' => false,
        'status' => 404,
        'message' => 'the player is not owner the room',
      ]);
    }
    return app('App\Http\Controllers\Api\V1\Product\PurchaseController')->getValidPurchase($roomId, $request);
  }

}