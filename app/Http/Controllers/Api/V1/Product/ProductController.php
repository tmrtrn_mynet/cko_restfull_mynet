<?php

namespace App\Http\Controllers\Api\V1\Product;

use App\Models\Product\Product;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use Swagger\Annotations as SWG;

class ProductController extends Controller {

/**
 * @SWG\Get(
 *   path="/product",
 *   tags={"product"},
 *   operationId="showAll",
 *   summary="list products",
 *   produces={"application/json"},
 *   @SWG\Parameter(
 * 	   name="category",
 *     in="query",
 * 		 type="name",
 * 		 description="ex:category names=vipistaka,useristaka,...",
 *     required=false,
 * 	 ),
 *   @SWG\Parameter(
* 	   name="keywords",
*      in="query",
* 		 type="string",
*      required=false,
* 		 description="returns search results for SKU",
* 	 ),
*   @SWG\Parameter(
* 	   name="active",
*      in="query",
* 		 type="boolean",
*      required=false,
* 		 description="selects active products",
* 	 ),
*   @SWG\Response(
* 		status=200,
* 	    description="success",
* 		  @SWG\Schema(
*          @SWG\Property(property="data", ref="#/definitions/Product")
*      ),
* 	 ),
*   @SWG\Response(
*     response="default",
*     description="an ""unexpected"" error"
*   ),
*   security={
*       {"Admin_Bearer": {}}
*   }
* )
*/
  public function showAll(Request $request)
  {
    $query = Product::whereHas('Category', function($query) use ($request) {
      if ($request->has('category')) {
        $categories = explode(',',$request->category);
        $query->whereIn("name", $categories);
      }
    });
    $query = $query->with('Category');
    if ($request->has('keywords')) {
      $query = $query -> search($request->keywords);
    }
    if ($request->has('active')) {
      $query = $query -> where("IsActive", $request->active);
    }

    return response()->json($query->get(), 200, [], JSON_UNESCAPED_UNICODE);
  }
  
/**
 * @SWG\Post(path="/product",
 *   tags={"product"},
 *   summary="Creates a product",
 *   description="This can only be done by the logged in user.",
 *   operationId="create",
 *   produces={"application/xml", "application/json"},
 *   @SWG\Parameter(
 *     in="body",
 *     name="body",
 *     description="Product Model",
 *     required=true,
 *     @SWG\Schema(ref="#/definitions/Product")
 *   ),
 * 	  @SWG\Response(
 * 		status="default",
 * 		description="error",
 * 		@SWG\Schema(ref="#/definitions/Error"),
 * 	  ),
 *    security={
 *       {"Admin_Bearer": {}}
 *   }
 * )
*/
  public function create(Request $request)
  {
    $obj = Product::create($request->all());
    return response()->json($obj, 201, [], JSON_UNESCAPED_UNICODE);
  }

/**
 * @SWG\Get(
 * 		path="/product/{sku}",
 * 		tags={"product"},
 * 		operationId="show",
 * 		summary="Gets product by sku",
 * 		@SWG\Parameter(
 * 			name="sku",
 * 			in="path",
 * 			required=true,
 * 			type="string",
 * 			description="product sku",
 * 		),
 * 		@SWG\Response(
 * 			status=200,
 * 			description="success",
 * 			@SWG\Schema(
 *        @SWG\Property(property="data", ref="#/definitions/Product")
 *      ),
 * 		),
 * 		@SWG\Response(
 * 			status="default",
 * 			description="error",
 * 			@SWG\Schema(ref="#/definitions/Error"),
 * 		),
 *    security={
 *       {"Admin_Bearer": {}}
 *    }
 * 	)
 *
 */
  public function show($sku)
  {
    return response()->json(Product::with('Category')->where('Sku', $sku)->first(), 200, [], JSON_UNESCAPED_UNICODE);
  }

/**
 * @SWG\Put(path="/product/{sku}",
 *   tags={"product"},
 *   summary="Updates th eproduct",
 *   description="This can only be done by the logged in user.",
 *   operationId="update",
 *   produces={"application/xml", "application/json"},
 *   @SWG\Parameter(
 *     name="sku",
 *     in="path",
 *     description="sku that need to be updated",
 *     required=true,
 *     type="string"
 *   ),
 *   @SWG\Parameter(
 *     in="body",
 *     name="Product",
 *     description="Update product object",
 *     @SWG\Schema(ref="#/definitions/Product")
 *   ),
 *   @SWG\Response(response=400, description="Invalid sku supplied"),
 *   @SWG\Response(response=404, description="Product not found"),
 *   security={
 *       {"Admin_Bearer": {}}
 *   }
 * )
 */
  public function update($sku, Request $request)
  {
    $obj = Product::where('Sku', $sku)->first();
    $obj->update($request->all());
    return response()->json($obj, 200, [], JSON_UNESCAPED_UNICODE);
  }

}