<?php

namespace App\Http\Controllers\Api\V1\Product;

use App\Models\Product\RoomProduct;
use App\Models\Product\Product;
use App\Models\Product\ProductPurchased;
use App\Utils;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use Swagger\Annotations as SWG;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class PurchaseController extends Controller {

/**
 * @SWG\Get(
 *   path="/purchase",
 *   tags={"purchase"},
 *   operationId="showAll",
 *   summary="list purchases",
 *   produces={"application/json"},
 *   @SWG\Parameter(
 * 	   name="category",
 *     in="query",
 * 		 type="string",
 * 		 description="(split with comma) example value:'vipistaka,useristaka,...'",
 *     required=false,
 * 	 ),
 *   @SWG\Parameter(
* 	   name="sku",
*      in="query",
* 		 type="string",
*      required=false,
* 		 description="(split with comma) example value:'26_istaka_7,26_oda_30'",
* 	 ),
*    @SWG\Parameter(
* 	   name="duration",
*      in="query",
* 		 type="string",
*      required=false,
* 		 description="selects product valid for, example value:'7,30' (split with comma)",
* 	 ),
 *   @SWG\Parameter(
* 	   name="token",
*      in="query",
* 		 type="string",
*      required=false,
* 		 description="search for Transaction Token",
* 	 ),
*   @SWG\Parameter(
* 	   name="status",
*      in="query",
* 		 type="string",
*      required=false,
* 		 description="TODO:Confirmed, Pending, Failed",
* 	 ),
*   @SWG\Parameter(
* 	   name="belongsTo",
*      in="query",
* 		 type="string",
*      required=false,
* 		 description="RoomIds or usernames (split with comma) example value:'5737,5677,layk1,...'",
* 	 ),
*   @SWG\Response(
* 		status=200,
* 	    description="success",
* 		  @SWG\Schema(
*          @SWG\Property(property="data", ref="#/definitions/Product")
*      ),
* 	 ),
*   @SWG\Response(
*     response="default",
*     description="an ""unexpected"" error"
*   ),
*   security={
*       {"Admin_Bearer": {}}
*   }
* )
*/
  public function showAll(Request $request)
  {
    $query = ProductPurchased::whereHas('product', function($query) use ($request) {
      if ($request->has('category')) {
        $categories = explode(',',$request->category);
        $query->whereIn("Category", $categories);
      }
      if ($request->has('sku')) {
        $skuList = explode(',',$request->sku);
        $query->whereIn("Sku", $skuList);
      }
    });
    if ($request->has('duration')) {
      $duration = explode(',',$request->duration);
      $query->whereIn("duration", $duration);
    }
    if ($request->has('token')) {
      $query->search($request->token);
    }
    if ($request->has('status')) {
      $query->where('Status', $request->status);
    }
    if ($request->has('belongsTo')) {
      $belongsToList = explode(',',$request->belongsTo);
      $query->whereIn("BelongsTo", $belongsToList);
    }

    return response()->json($query->get(), 200, [], JSON_UNESCAPED_UNICODE);
  }

/**
 * @SWG\Post(path="/purchase",
 *   tags={"purchase"},
 *   summary="Creates a purchased product",
 *   description="This can only be done by the logged in user.",
 *   operationId="create",
 *   produces={"application/xml", "application/json"},
 *   @SWG\Parameter(
 *     in="body",
 *     name="body",
 *     description="Purchase Model",
 *     required=true,
 *     @SWG\Schema(ref="#/definitions/ProductPurchased")
 *   ),
 * 	  @SWG\Response(
 * 		status="default",
 * 		description="error",
 * 		@SWG\Schema(ref="#/definitions/Error"),
 * 	  ),
 * )
*/
  public function create(Request $request)
  {
    //migration
    /*
    truncate table product_purchased
    alter table product_purchased ADD "StartDate" datetime not null
    CustomRoomData managerooms sayfasından DataKey, DataValue, ***yeni!!! ProductSku**** set edilecek
    */

    Utils::CheckWebsericeSecurity();

    $success = false;
    if (!$request->has(['sku', 'belongsTo', 'duration', 'owner', 'transactionToken', 'paymentType', 'purchasePrice'])) {
      return response()->json([
        'success' => $success,
        'status' => 500,
        'message' => 'missing parameter',
      ]);
    }
    $product = Product::where('Sku', $request->sku)->first();
    if (!$product) {
      return response()->json([
        'error' => [
            'message' => 'Sku not found',
        ]
      ]);
    }
    if (!is_numeric($request->duration) || $request->duration < 1) {
      return response()->json([
        'success' => $success,
        'status' => 500,
        'message' => 'insufficient or invalid duration: '.$request->duration,
      ]);
    }
    
    try {
      $purchasedItem = ProductPurchased::where('TransactionToken', '=', Input::get('transactionToken'))->first();
      if ($purchasedItem) {
        // transaction exist, check product
        if ($purchasedItem->Status !== 'Pending') {
          return response()->json([
            'success' => $success,
            'status' => 500,
            'message' => "Uzgunuz, bu urun daha once alinmistir.",
          ]);
        }
      } else {
        $purchasedBefore = ProductPurchased::where([
          ['ProductSku', $product->Sku],
          ['Status', 'Confirmed'],
          ['BelongsTo', $request->belongsTo]
        ])->whereDate('ExpireDate', '>', Utils::GetCurrentDateTimeFormat())
        ->latest('ExpireDate')->first();

        $purchasedItem = new ProductPurchased;

        if ($purchasedBefore) {
          $StartDate = $purchasedBefore->ExpireDate;
          $date = \DateTime::createFromFormat($purchasedBefore->dateFormat, $StartDate);
          $ExpireDate = $date->modify('+'.$request->duration.' day')->format($purchasedBefore->dateFormat);
        } else {
          $StartDate = Utils::GetCurrentDateTimeFormat();
          $ExpireDate = Utils::GetCurrentDateTime()->modify('+'.$request->duration.' day')->format($purchasedItem->dateFormat);
        }

        
        $purchasedItem->ProductSku = $product->Sku;
        $purchasedItem->BelongsTo = $request->belongsTo;
        $purchasedItem->ExpireDate = $ExpireDate;
        $purchasedItem->StartDate = $StartDate;
        $purchasedItem->Owner = $request->owner;
        $purchasedItem->Duration = $request->duration;
        $purchasedItem->TransactionToken = $request->transactionToken;
        $purchasedItem->Quantity = $request->has('quantity') ? $request->quantity : 1;
        $purchasedItem->IsTrial = $request->has('isTrial') ? $request->isTrial : 0;
        $purchasedItem->CustomData = $request->has('customData') ? $request->customData : "";
        $purchasedItem->PurchasePrice = $request->purchasePrice;
        $purchasedItem->PaymentType = $request->paymentType;
        
      }
      $purchasedItem->Status = 'Confirmed';
      $saved = $purchasedItem->save();
      if($saved == null){
        return response()->json([
          'success' => $success,
          'status' => 500,
          'message' => "purchase item couldn't created.",
        ]);
      } else {
        $success = true;
      }
      
      return response()->json([
        'success' => $success,
        'status' => 200,
        'message' => "",
      ]);
    } catch(\Illuminate\Database\QueryException $ex) {
      return response()->json([
        'success' => $success,
        'status' => 500,
        'message' => 'query error '.$request->all(),
      ]);
    }
    
  }

/**
 * @SWG\Put(path="/purchase/{token}",
 *   tags={"purchase"},
 *   summary="Updates a purchased item",
 *   description="This can only be done by the logged in user.",
 *   operationId="updatePurchasedItem",
 *   produces={"application/xml", "application/json"},
 *   @SWG\Parameter(
 *     name="token",
 *     in="path",
 *     description="Change Status of ProductPurchased by token - orderid",
 *     required=true,
 *     type="string"
 *   ),
 *   @SWG\Parameter(
* 	   name="status",
*      in="query",
* 		 type="string",
*      required=true,
* 		 description="Canceled, Failed",
* 	 ),
 *   @SWG\Response(response=400, description="Invalid token supplied"),
 *   @SWG\Response(response=404, description="PurchasedItem not found")
 * )
 */
  public function updatePurchasedItem($token, Request $request)
  {
    Utils::CheckWebsericeSecurity();

    // TODO: BROKEN status degistiginde expire-start dateleri de tekrar degisrimen gerekiyor.
    $success = false;
    $message = "";
    try {
      $allowedStatus = array('Confirmed','Canceled', 'Failed');
      if (in_array(Input::get('status'), $allowedStatus)) {
        $status       =  Input::get('status');
        $productPurchased = ProductPurchased::where('TransactionToken', $token)->firstOrFail();
        $activeStatus = $productPurchased->Status;
       // die("".strcasecmp($activeStatus, $status) == 0 ? "esit" : "degil");
        if (strcasecmp($activeStatus, $status) == 0) {
          // nothing changed
          return response()->json([
            'success' => true,
            'status' => 200,
            'message' => "equal status",
          ]);
        }
        $message = "status:".$status;
        if (strcasecmp($activeStatus, "Confirmed") == 0) {
          if (strcasecmp($status, "Canceled") == 0 || strcasecmp($status, "Failed") == 0) {
            // iptal durumunda, iptal edilenden sonra ayni sku ile satin alinmıs varsa
            // onlarin expiredate ve startdate 'leri geri cekilir
            
            $expiredatetime = Utils::GetDateTime($productPurchased->ExpireDate);
            $now = Utils::GetCurrentDateTime();
            $dteDiff  = $expiredatetime->diff($now)->days;
            if ($dteDiff <= 0) {
              return response()->json([
                'success' => true,
                'status' => 200,
                'message' => "nothing changed it has been already expired ". $dteDiff . " days",
              ]);
            }

            if ($dteDiff > $productPurchased->Duration) {
              // urun daha kullanilmamis, max duration 'i dusecegiz.
              $dteDiff = $productPurchased->Duration;
            }

            $message = "dteDiff:".$dteDiff;

            $purchasedAfter = ProductPurchased::where([
              ['ProductSku', $productPurchased->ProductSku],
              ['Status', 'Confirmed'],
              ['BelongsTo', $productPurchased->BelongsTo]
            ])->whereDate('ExpireDate', '>', Utils::GetCurrentDateTimeFormat())
            ->whereDate('ExpireDate', '>', $productPurchased->ExpireDate)->get();
           /* ->update(
              ['StartDate' => DB::raw('DATEADD(DAY, -'.$dteDiff.', "StartDate")')],
              ['ExpireDate' => DB::raw('DATEADD(DAY, -'.$dteDiff.', "ExpireDate")')]
            ); */
            foreach ($purchasedAfter as $purchase) {
              $dateStart = \DateTime::createFromFormat($purchase->dateFormat, $purchase->StartDate);
              $StartDateNew = $dateStart->modify('-'.$dteDiff.' day')->format($purchase->dateFormat);
              $dateExpire = \DateTime::createFromFormat($purchase->dateFormat, $purchase->ExpireDate);
              $ExpireDateNew = $dateExpire->modify('-'.$dteDiff.' day')->format($purchase->dateFormat);
              $purchase->StartDate = $StartDateNew;
              $purchase->ExpireDate = $ExpireDateNew;
              $purchase->save();
            }
           
            $productPurchased->Status = $status;
            $saved = $productPurchased->save();

          }
        }
        
        if ($saved) {
          $success = true;
        } else {
          $message = "couldn't saved token: ".$token;
        }
      } else {
        $message = "invalid status";
      }
    } catch(\Illuminate\Database\QueryException $ex) {
      $success = false;
      $message = $ex->getMessage();
    }

    return response()->json([
      'success' => $success,
      'status' => $success ? 200 : 500,
      'message' => $message,
    ]);
  }

/**
 * @SWG\Get(
 * 		path="/validpurchase/{belongsTo}",
 * 		tags={"purchase"},
 * 		operationId="validpurchase",
 * 		summary="Gets active purchased items with belongsTo",
 * 		@SWG\Parameter(
 * 			name="belongsTo",
 * 			in="path",
 * 			required=true,
 * 			type="string",
 * 			description="belongsTO roomid or username",
 * 		),
 *    @SWG\Parameter(
* 	   name="sku",
*      in="query",
* 		 type="string",
*      required=false,
* 		 description="sku list ex:istakasb,istakask",
* 	 ),
 * 		@SWG\Response(
 * 			status=200,
 * 			description="success",
 * 			@SWG\Schema(
 *        @SWG\Property(property="data", ref="#/definitions/RoomProduct")
 *      ),
 * 		),
 * 		@SWG\Response(
 * 			status="default",
 * 			description="error",
 * 			@SWG\Schema(ref="#/definitions/Error"),
 * 		),
 * 	)
 *
 */
  public function validpurchase($belongsTo, Request $request) {

    Utils::CheckWebsericeSecurity();
    return $this->getValidPurchase($belongsTo, $request);
  }

  public function getValidPurchase($belongsTo, Request $request) {
    $success = false;
    $message = "";
    try {
      if($request->has(['sku'])) {
        $skuList = explode(',',$request->sku);
        $products = Product::where('IsActive', true)->whereIn("Sku", $skuList);
    /*    if ($products->count() < count($skuList)) {
          return response()->json([
            'success' => $success,
            'status' => 500,
            'message' => "one of the product or products doesn't exist or deprecated sku:".$sku,
          ]);
        } */
      }

      $purchased = ProductPurchased::select(DB::raw('ProductSku, max(ExpireDate) as ExpireDate'))
      ->where([
        ['Status', '=', 'Confirmed'],
        ['BelongsTo',  '=', $belongsTo],
        ['ExpireDate', '>', Utils::GetCurrentDateTimeFormat()]
      ]);
      if (isset($skuList)) {
        $purchased->whereIn("ProductSku", $skuList);
      }
      $purchased->groupBy("ProductSku");

      $success = true;
      $message = $purchased->get();

    } catch(\Illuminate\Database\QueryException $ex) {
      $success = false;
      $message = $ex->getMessage();
    }
    return response()->json([
      'success' => $success,
      'status' => $success ? 200 : 500,
      'message' => $message,
    ]);
  }


 /**
 * @SWG\Get(
 * 		path="/validpurchasesByTokens",
 * 		tags={"purchase"},
 * 		operationId="validpurchase",
 * 		summary="Gets active purchased items tokens",
 *    @SWG\Parameter(
 * 	   name="tokens",
 *      in="query",
 * 		 type="string",
 *      required=false,
 * 		 description="token list ex:1234,9876",
 * 	 ),
 * 		@SWG\Response(
 * 			status=200,
 * 			description="success",
 * 			@SWG\Schema(
 *        @SWG\Property(property="data", ref="#/definitions/ProductPurchased")
 *      ),
 * 		),
 * 		@SWG\Response(
 * 			status="default",
 * 			description="error",
 * 			@SWG\Schema(ref="#/definitions/Error"),
 * 		),
 * 	)
 *
 */
  public function validpurchasesByTokens(Request $request) {
    Utils::CheckWebsericeSecurity();

    $success = false;
    $rows = array();
    try {
      if($request->has(['tokens'])) {
        $tokenList = explode(',',$request->tokens);
        $purchased = ProductPurchased::select('ProductSku','TransactionToken')
          ->whereIn("TransactionToken", $tokenList);
        $success = true;
        $rows = $purchased->get();
      } else {
        $rows = "missing tokens";
      }
    } catch(\Illuminate\Database\QueryException $ex) {
      $success = false;
      $rows = $ex->getMessage();
    }
    return response()->json([
      'success' => $success,
      'status' => $success ? 200 : 500,
      'message' => json_encode($rows),
    ]);
  }

}