<?php

namespace App\Http\Controllers\Api\V1\Product;

use App\Models\Product\ProductCategory;
use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use Swagger\Annotations as SWG;

class GroupController extends Controller {

/**
 * @SWG\Get(
 *   path="/productgroups",
 *   tags={"product category"},
 *   operationId="showAll",
 *   summary="list product groups",
 *   produces={"application/json"},
 *   @SWG\Response(
 * 		status=200,
 * 	    description="success",
 * 		  @SWG\Schema(
 *          @SWG\Property(property="data", ref="#/definitions/ProductCategory")
 *      ),
 * 	 ),
 *   @SWG\Response(
 *     response="default",
 *     description="an ""unexpected"" error"
 *   ),
 *    security={
 *       {"Admin_Bearer": {}}
 *     }
 * )
 */
public function showAll()
{
    return response()->json(ProductCategory::all(), 200, [], JSON_UNESCAPED_UNICODE);
}

/**
 * @SWG\Get(
 * 		path="/productgroups/{category}",
 * 		tags={"product category"},
 * 		operationId="show",
 * 		summary="Gets category by name",
 * 		@SWG\Parameter(
 * 			name="name",
 * 			in="path",
 * 			required=true,
 * 			type="string",
 * 			description="category name",
 * 		),
 * 		@SWG\Response(
 * 			status=200,
 * 			description="success",
 * 			@SWG\Schema(
 *        @SWG\Property(property="data", ref="#/definitions/ProductCategory")
 *      ),
 * 		),
 * 		@SWG\Response(
 * 			status="default",
 * 			description="error",
 * 			@SWG\Schema(ref="#/definitions/Error"),
 * 		),
 *    security={
 *       {"Admin_Bearer": {}}
 *     }
 * 	)
 *
 */
public function show($name)
{
  return response()->json(ProductCategory::find($name), 200, [], JSON_UNESCAPED_UNICODE);
}


/**
 * @SWG\Post(path="/productgroups",
 *   tags={"product category"},
 *   summary="Creates a product group",
 *   description="This can only be done by the logged in user.",
 *   operationId="create",
 *   produces={"application/xml", "application/json"},
 *   @SWG\Parameter(
 *     in="body",
 *     name="body",
 *     description="Created game object",
 *     required=true,
 *     @SWG\Schema(ref="#/definitions/ProductCategory")
 *   ),
 * 	  @SWG\Response(
 * 		status="default",
 * 		description="error",
 * 		@SWG\Schema(ref="#/definitions/Error"),
 * 	  ),
 *   security={
 *       {"Admin_Bearer": {}}
 *   }
* )
*/
public function create(Request $request)
{
  $category = ProductCategory::create($request->all());
  return response()->json($category, 201, [], JSON_UNESCAPED_UNICODE);
}

/**
 * @SWG\Put(path="/productgroups/{name}",
 *   tags={"product category"},
 *   summary="Updates a product group",
 *   description="This can only be done by the logged in user.",
 *   operationId="update",
 *   produces={"application/xml", "application/json"},
 *   @SWG\Parameter(
 *     name="name",
 *     in="path",
 *     description="name id that need to be updated",
 *     required=true,
 *     type="string"
 *   ),
 *   @SWG\Parameter(
 *     in="body",
 *     name="ProductCategory",
 *     description="Update a category object",
 *     @SWG\Schema(ref="#/definitions/ProductCategory")
 *   ),
 *   @SWG\Response(response=400, description="Invalid name supplied"),
 *   @SWG\Response(response=404, description="Category not found"),
 *   security={
 *       {"Admin_Bearer": {}}
 *   }
 * )
 */
  public function update($name, Request $request)
  {
    $category = ProductCategory::findOrFail($name);
    $category->update($request->all());
    return response()->json($category, 200, [], JSON_UNESCAPED_UNICODE);
  }




}