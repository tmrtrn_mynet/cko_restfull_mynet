<?php
namespace App\Http\Controllers\Api\V1\Site;

use App\Http\Controllers\Controller as Controller;
use App\SitePage;
use App\SiteWidget;
use Illuminate\Http\Request;
use Swagger\Annotations as SWG;

class SiteController extends Controller {

/**
* @SWG\Get(
*   path="/site",
*   tags={"site"},
*   summary="list sites",
*   produces={"application/json"},
*   @SWG\Response(
* 		status=200,
* 	  description="success",
* 		@SWG\Schema(
*          @SWG\Property(property="data", ref="#/definitions/SitePage")
*      ),
* 	 ),
*   @SWG\Response(
*     response="default",
*     description="an ""unexpected"" error"
*   )
* )
*/
  public function showAllPages() {
    return response()->json(SitePage::all(), 200, [], JSON_UNESCAPED_UNICODE);
  }

/**
 * @SWG\Post(path="/site",
 *   tags={"site"},
 *   summary="Create a page",
 *   description="Pages on mynet oyun",
 *   operationId="createPage",
 *   produces={"application/xml", "application/json"},
 *   @SWG\Parameter(
 *     in="body",
 *     name="body",
 *     description="Created page object",
 *     required=true,
 *     @SWG\Schema(ref="#/definitions/SitePage")
 *   ),
 * 	  @SWG\Response(
 * 		status="default",
 * 		description="error",
 * 		@SWG\Schema(ref="#/definitions/Error"),
 * 	  ),
 * )
 */
  public function createPage(Request $request)
  {
    $page = SitePage::create($request->all());
    return response()->json($page, 201, [], JSON_UNESCAPED_UNICODE);
  }

/**
 * @SWG\Get(
 * 		path="/site/{id}",
 * 		tags={"site"},
 * 		operationId="showOneSitePage",
 * 		summary="Fetch site details",
 * 		@SWG\Parameter(
 * 			name="id",
 * 			in="path",
 * 			required=true,
 * 			type="string",
 * 			description="site id",
 * 		),
 * 		@SWG\Response(
 * 			status=200,
 * 			description="success",
 * 			@SWG\Schema(
 *        @SWG\Property(property="data", ref="#/definitions/SitePage")
 *      ),
 * 		),
 * 		@SWG\Response(
 * 			status="default",
 * 			description="error",
 * 			@SWG\Schema(ref="#/definitions/Error"),
 * 		),
 * 	)
 *
 */
  public function showOneSitePage($id)
  {
      return response()->json(SitePage::find($id), 200, [], JSON_UNESCAPED_UNICODE);
  }


/**
 * @SWG\Put(path="/site/{id}",
 *   tags={"site"},
 *   summary="Updated page",
 *   description="This can only be done by the logged in user.",
 *   operationId="updatePage",
 *   produces={"application/xml", "application/json"},
 *   @SWG\Parameter(
 *     name="id",
 *     in="path",
 *     description="page id that need to be updated",
 *     required=true,
 *     type="integer"
 *   ),
 *   @SWG\Parameter(
 *     in="body",
 *     name="SitePage",
 *     description="Update page object",
 *     @SWG\Schema(ref="#/definitions/SitePage")
 *   ),
 *   @SWG\Response(response=400, description="Invalid page supplied"),
 *   @SWG\Response(response=404, description="Page not found")
 * )
 */
    public function updatePage($id, Request $request)
    {
      $page = SitePage::findOrFail($id);
      $page->update($request->all());

      return response()->json($page, 200, [], JSON_UNESCAPED_UNICODE);
    }

  /**
   * @SWG\Delete(path="/site/{id}",
   *   tags={"site"},
   *   summary="Delete site",
   *   description="This can only be done by the logged in user.",
   *   operationId="deletePage",
   *   produces={"application/xml", "application/json"},
   *   @SWG\Parameter(
   *     name="id",
   *     in="path",
   *     description="page id that needs to be deleted",
   *     required=true,
   *     type="integer"
   *   ),
   *   @SWG\Response(response=400, description="Invalid page id supplied"),
   *   @SWG\Response(response=404, description="Page not found")
   * )
   */
  public function deletePage($id)
  {
    SitePage::findOrFail($id)->delete();
    return response('Deleted Successfully', 200);
  }

/**
 * @SWG\Post(path="/widget",
 *   tags={"widget"},
 *   summary="Create a widget data",
 *   description="Create a Widget data on mynet oyun pages",
 *   operationId="create",
 *   produces={"application/xml", "application/json"},
 *   @SWG\Parameter(
 * 	   name="page_id",
 *     required=true,
 *     in="query",
 * 		 type="integer",
 * 		 description="page_id associated with site_page",
 * 	 ),
 *  @SWG\Parameter(
 * 	   name="title",
 *     required=true,
 *     in="query",
 * 		 type="string",
 * 		 description="title",
 * 	 ),
 *   @SWG\Parameter(
 * 	   name="customData",
 * 		 type="object",
 *     in="query",
 * 		 description="json object",
 *     defaultValue={}
 * 	 ),
 * 	  @SWG\Response(
 * 		status="default",
 * 		description="error",
 * 		@SWG\Schema(ref="#/definitions/Error"),
 * 	  ),
 * )
 */
  public function createWidget(Request $request)
  {
    $widget = SiteWidget::create($request->all());
    return response()->json($widget, 201, [], JSON_UNESCAPED_UNICODE);
  }

  /**
   * @SWG\Delete(path="/widget/{id}",
   *   tags={"widget"},
   *   summary="Delete widget",
   *   description="This can only be done by the logged in user.",
   *   operationId="deleteWidget",
   *   produces={"application/xml", "application/json"},
   *   @SWG\Parameter(
   *     name="id",
   *     in="path",
   *     description="Widget id that needs to be deleted",
   *     required=true,
   *     type="integer"
   *   ),
   *   @SWG\Response(response=400, description="Invalid widget id supplied"),
   *   @SWG\Response(response=404, description="Widget not found")
   * )
   */
  public function deleteWidget($id)
  {
    SiteWidget::findOrFail($id)->delete();
    return response('Deleted Successfully', 200);
  }


/**
 * @SWG\Get(
 * 		path="/widget/{id}",
 * 		tags={"widget"},
 * 		operationId="getOneWidget",
 * 		summary="Fetch widget details",
 * 		@SWG\Parameter(
 * 			name="id",
 * 			in="path",
 * 			required=true,
 * 			type="integer",
 * 			description="widget id",
 * 		),
 * 		@SWG\Response(
 * 			status=200,
 * 			description="success",
 * 			@SWG\Schema(
 *        @SWG\Property(property="data", ref="#/definitions/SiteWidget")
 *      ),
 * 		),
 * 		@SWG\Response(
 * 			status="default",
 * 			description="error",
 * 			@SWG\Schema(ref="#/definitions/Error"),
 * 		),
 * 	)
 *
 */
  public function getOneWidget($id) {
    return response()->json(SiteWidget::find($id), 200, [], JSON_UNESCAPED_UNICODE);
  }

  /**
* @SWG\Get(
*   path="/widget",
*   tags={"widget"},
*   summary="list widgets",
*   produces={"application/json"},
*   @SWG\Response(
* 		status=200,
* 	  description="success",
* 		@SWG\Schema(
*          @SWG\Property(property="data", ref="#/definitions/SiteWidget")
*      ),
* 	 ),
*   @SWG\Response(
*     response="default",
*     description="an ""unexpected"" error"
*   )
* )
*/
  public function showAllWidgets() {
    return response()->json(SiteWidget::all(), 200, [], JSON_UNESCAPED_UNICODE);
  }

  /**
 * @SWG\Put(path="/widget/{id}",
 *   tags={"widget"},
 *   summary="Updated widget",
 *   description="This can only be done by the logged in user.",
 *   operationId="updateWidget",
 *   produces={"application/xml", "application/json"},
 *   @SWG\Parameter(
 *     name="id",
 *     in="path",
 *     description="widget id that need to be updated",
 *     required=true,
 *     type="integer"
 *   ),
 *   @SWG\Parameter(
 *     in="body",
 *     name="SiteWidget",
 *     description="Update widget object",
 *     @SWG\Schema(ref="#/definitions/SiteWidget")
 *   ),
 *   @SWG\Response(response=400, description="Invalid page supplied"),
 *   @SWG\Response(response=404, description="Page not found")
 * )
 */
  public function updateWidget($id, Request $request)
  {
    $widget = SiteWidget::findOrFail($id);
    $widget->update($request->all());

    return response()->json($widget, 200, [], JSON_UNESCAPED_UNICODE);
  }


}

