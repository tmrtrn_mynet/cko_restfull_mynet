<?php

namespace App\Http\Controllers\Api\V1\Site;

use App\Http\Controllers\Controller as Controller;
use App\Game;
use Illuminate\Http\Request;
use Swagger\Annotations as SWG;

class GameController extends Controller
{
    /**
    * @SWG\Get(
    *   path="/games",
    *   tags={"games"},
    *   summary="list games",
    *   produces={"application/json"},
    *   @SWG\Response(
	* 		status=200,
	* 	    description="success",
	* 		@SWG\Schema(
    *          @SWG\Property(property="data", ref="#/definitions/Game")
    *      ),
	* 	 ),
    *   @SWG\Response(
    *     response="default",
    *     description="an ""unexpected"" error"
    *   )
    * )
    */
    public function showAllGames()
    {
        return response()->json(Game::all(), 200, [], JSON_UNESCAPED_UNICODE);
    }

	 /**
	 * @SWG\Get(
	 * 		path="/games/{id}",
	 * 		tags={"games"},
	 * 		operationId="getGame",
	 * 		summary="Fetch game details",
	 * 		@SWG\Parameter(
	 * 			name="id",
	 * 			in="path",
	 * 			required=true,
	 * 			type="string",
	 * 			description="DD_GAME_ID",
	 * 		),
	 * 		@SWG\Response(
	 * 			status=200,
	 * 			description="success",
	 * 			@SWG\Schema(
     *              @SWG\Property(property="data", ref="#/definitions/Game")
     *          ),
	 * 		),
	 * 		@SWG\Response(
	 * 			status="default",
	 * 			description="error",
	 * 			@SWG\Schema(ref="#/definitions/Error"),
	 * 		),
	 * 	)
	 *
	 */
    public function showOneGame($id)
    {
        return response()->json(Game::find($id), 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * @SWG\Post(path="/games",
     *   tags={"games"},
     *   summary="Create game",
     *   description="This can only be done by the logged in user.",
     *   operationId="create",
     *   produces={"application/xml", "application/json"},
     *   @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="Created game object",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/Game")
     *   ),
     * 	  @SWG\Response(
	 * 		status="default",
	 * 		description="error",
	 * 		@SWG\Schema(ref="#/definitions/Error"),
	 * 	  ),
     * )
     */
    public function create(Request $request)
    {
        $game = Game::create($request->all());

        return response()->json($game, 201, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * @SWG\Put(path="/games/{DD_GAME_ID}",
     *   tags={"games"},
     *   summary="Updated game",
     *   description="This can only be done by the logged in user.",
     *   operationId="update",
     *   produces={"application/xml", "application/json"},
     *   @SWG\Parameter(
     *     name="DD_GAME_ID",
     *     in="path",
     *     description="name that need to be updated",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     in="body",
     *     name="body",
     *     description="Updated game object",
     *     required=true,
     *     @SWG\Schema(ref="#/definitions/Game")
     *   ),
     *   @SWG\Response(response=400, description="Invalid game supplied"),
     *   @SWG\Response(response=404, description="Game not found")
     * )
     */
    public function update($id, Request $request)
    {
        $game = Game::findOrFail($id);
        $game->update($request->all());

        return response()->json($game, 200, [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * @SWG\Delete(path="/games/{DD_GAME_ID}",
     *   tags={"games"},
     *   summary="Delete game",
     *   description="This can only be done by the logged in user.",
     *   operationId="delete",
     *   produces={"application/xml", "application/json"},
     *   @SWG\Parameter(
     *     name="DD_GAME_ID",
     *     in="path",
     *     description="The name that needs to be deleted",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(response=400, description="Invalid DD_GAME_ID supplied"),
     *   @SWG\Response(response=404, description="Game not found")
     * )
     */
    public function delete($id)
    {
        Game::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
