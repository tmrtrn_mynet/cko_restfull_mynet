<?php

namespace App\Transformers;

use App\Models\User;
use App\Models\OverallScores;
use League\Fractal\TransformerAbstract;
use League\Fractal\ParamBag;

class UserTransformer extends TransformerAbstract
{
    protected $authorization;
    protected $availableIncludes = ['details', 'scores', 'achievements'];

    public function transform(User $user)
    {

        return [
            'username' => $user->USERNICK,
        ];
       // return $user->attributesToArray();
    }

    public function includeDetails(User $user) {
        return $this->item($user, new UserDetailsTransformer());
    }

    public function includeScores(User $user, ParamBag $params = null)
    {
        if (!$user->scores) {
            //return $this->item(new OverAllScores(), new ScoreTransformer());
        }
        $selectedGameId = 'ok';
        if ($params->get('gameId')) {
            $selectedGameId = current((array)$params->get('gameId'));
        }
        $score = OverallScores::where('DD_GAME_ID', $selectedGameId)->where('USERNICK', $user->USERNICK)->first();
        return $this->item($score, new ScoreTransformer());
    }

    public function includeAchievements(User $user, ParamBag $params = null) {
        $selectedGameId = 'ok';
        if ($params->get('gameId')) {
            $selectedGameId = current((array)$params->get('gameId'));
        }
        $result = app('App\Http\Controllers\Api\V1\Achievement\AchievementController')->getUserAchievements($user->USERNICK, $selectedGameId);
        return $this->collection($result, new AchievementTransformer)
            ->setMeta(['count' => $result->count()]);
    }

    public function setAuthorization($authorization)
    {
        $this->authorization = $authorization;

        return $this;
    }

    public function includeAuthorization(User $user)
    {
        if (! $this->authorization) {
            return $this->null();
        }

        return $this->item($this->authorization, new AuthorizationTransformer());
    }
}
