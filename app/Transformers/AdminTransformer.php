<?php

namespace App\Transformers;

use App\Models\AdminAccount;
use League\Fractal\TransformerAbstract;

class AdminTransformer extends TransformerAbstract
{
    protected $authorization;

    public function transform(AdminAccount $admin)
    {
        return [
            'id' => $admin->id,
            'username' => $admin->username
        ];
    }

    public function setAuthorization($authorization)
    {
        $this->authorization = $authorization;

        return $this;
    }

    public function includeAuthorization(AdminAccount $admin)
    {
        if (! $this->authorization) {
            return $this->null();
        }

        return $this->item($this->authorization, new AuthorizationTransformer());
    }
}
