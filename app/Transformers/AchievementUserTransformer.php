<?php

namespace App\Transformers;

use App\Models\Achievement\Achievement_User;
use League\Fractal\ParamBag;
use League\Fractal\TransformerAbstract;

class AchievementUserTransformer extends TransformerAbstract
{

    public function transform(Achievement_User $userAchievement)
    {
        return $userAchievement->attributesToArray();
    }

}
