<?php

namespace App\Transformers;

use App\Models\Achievement\Achievement;
use League\Fractal\ParamBag;
use League\Fractal\TransformerAbstract;

class AchievementTransformer extends TransformerAbstract
{

    public function transform(Achievement $ach)
    {
        return [
            "Code" => $ach->Code,
            "Group" => $ach->Group,
            "Title" => $ach->Title,
            "Description" => $ach->Description,
            "Position" => $ach->Position,
            "Reward" => $ach->Reward,
            "TargetPoint" => $ach->TargetPoint,
            "CurrentPoint" => $ach->CurrentPoint,
            "RewardClaimed" => is_null($ach->RewardClaimed) || !$ach->RewardClaimed ? 0 : 1 
        ];
    }

}
