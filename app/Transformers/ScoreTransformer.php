<?php

namespace App\Transformers;

use App\Models\OverallScores;
use League\Fractal\TransformerAbstract;

class ScoreTransformer extends TransformerAbstract
{

    public function transform(OverallScores $score)
    {
        return [
           // 'username' => $score->USERNICK,
            'gameId' => $score->DD_GAME_ID,
            'levelId' => $score->DD_LEVEL_ID,
            'totalScore' => $score->TOTAL_SCORE,
            'won' => $score->GAMES_WON,
            'lost' => $score->GAMES_LOST,
            'experience'=> $score->GAME_EXPERIENCE,
            'abandoned' => $score->ABANDONED_GAMES,
            'drawn' => $score->GAMES_DRAWN
        ];
    }
}
