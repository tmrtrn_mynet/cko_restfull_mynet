<?php

namespace App\Transformers;

use App\Models\User;
use League\Fractal\TransformerAbstract;
use App\Utils;

class UserDetailsTransformer extends TransformerAbstract
{

    public function transform(User $user)
    {
      $deprecated_paid_expire = is_null($user->PAID_EXPIRE) ? Utils::GetDateTime("1900-01-01 00:00:00") : Utils::GetDateTime($user->PAID_EXPIRE);
      $deprecated_super_expire = is_null($user->SUPER_EXPIRE) ? Utils::GetDateTime("1900-01-01 00:00:00") : Utils::GetDateTime($user->SUPER_EXPIRE);
      $new_super_expire = is_null($user->NEW_SUPER_EXPIRE) ? Utils::GetDateTime("1900-01-01 00:00:00") : Utils::GetDateTime($user->NEW_SUPER_EXPIRE);
      $saved_diff_expire = is_null($user->DIFF_EXPIRE) ? Utils::GetDateTime("1900-01-01 00:00:00") : Utils::GetDateTime($user->DIFF_EXPIRE);
      $saved_charismatic_expire = is_null($user->CHARISMATIC_EXPIRE) ? Utils::GetDateTime("1900-01-01 00:00:00") : Utils::GetDateTime($user->CHARISMATIC_EXPIRE);
      $saved_mute_ban_expire = is_null($user->SUPERNICK_BAN_EXPIRE) ? Utils::GetDateTime("1900-01-01 00:00:00") : Utils::GetDateTime($user->MUTE_BAN_EXPIRE);
      $saved_supernick_ban_expire = is_null($user->SUPERNICK_BAN_EXPIRE) ? Utils::GetDateTime("1900-01-01 00:00:00") : Utils::GetDateTime($user->SUPERNICK_BAN_EXPIRE);

      $now = Utils::GetCurrentDateTime();

      $max_paid_expire = max($deprecated_paid_expire, $new_super_expire);
      $paid_expire = $max_paid_expire >  $now ? $max_paid_expire->format("Y-m-d H:i:s") : "";

      $max_diff_expire = max($saved_diff_expire, $new_super_expire, $saved_charismatic_expire);
      $diff_expire = $max_diff_expire > $now ? $max_diff_expire->format("Y-m-d H:i:s") : "";

      $max_super_expire = max($deprecated_super_expire, $new_super_expire);
      $super_expire = $max_super_expire >  $now ? $max_super_expire->format("Y-m-d H:i:s") : "";


      $mute_ban_expire = $saved_mute_ban_expire > $now ? $saved_mute_ban_expire->format("Y-m-d H:i:s") : "";
      $supernick_ban_expire = $saved_supernick_ban_expire > $now ? $saved_supernick_ban_expire->format("Y-m-d H:i:s") : "";

      $charismatic_expire = $saved_charismatic_expire > $now ? $saved_charismatic_expire->format("Y-m-d H:i:s") : "";
      
      $avatarCode = intval($user->CHARCODE);
      $avatarLobby = $user->AVATAR_LOBBY;
      $avatarGame = $user->AVATAR_IN_GAME;
      if (strlen($diff_expire) == 0) {
          $avatarLobby = 0;
          $avatarGame = 0;
          if($avatarCode > 21 && $avatarCode < 247) {
              $$avatarCode = 0;
          }
      }

      return [
          'avatar_code' => $avatarCode,
          'paid_expire' => $paid_expire,
          'personel_message' => is_null($user->PERSONEL_MESSAGE) ? "" : $user->PERSONEL_MESSAGE,
          'avatar_lobby' => $avatarLobby,
          'diff_expire' => $diff_expire,
          'avatar_game' => $avatarGame,
          'super_expire' => $super_expire,
          'supernick' => strlen($super_expire) > 0 && $user->USE_SUPERNICK == 1 && strlen($supernick_ban_expire) == 0 ? $user->SUPERNICK : "",
          'mute_ban_expire' => $mute_ban_expire,
          'supernick_ban_expire' => $supernick_ban_expire,
          'charismatic_expire'=> $charismatic_expire
      ];
    }
}
