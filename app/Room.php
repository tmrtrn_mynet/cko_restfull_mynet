<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Swagger\Annotations as SWG;

/**
 * @SWG\Definition(
 *     @SWG\Property(property="DD_GAME_ID", type="string"),
 *     @SWG\Property(property="ROOMID", type="integer"),
 *     @SWG\Property(property="ROOMNAME", type="string"),
 *     @SWG\Property(property="DD_MAX_MEMBERS", type="integer"),
 *     @SWG\Property(property="SERVER_ADDRESS", type="string"),
 *     @SWG\Property(property="PORT", type="integer"),
 *     @SWG\Property(property="PLAYER_COUNT", type="integer"),
 *     @SWG\Property(property="TYPE", type="integer"),
 *     @SWG\Property(property="ROOMNAMETR", type="string"),
 *     @SWG\Property(property="USER_COUNTER", type="integer"),
 *     @SWG\Property(property="CATINDEX", type="integer"),
 *     @SWG\Property(property="SCORE_MULTIPLIER", type="integer"),
 *     @SWG\Property(property="MARKET_ID", type="string"),
 *     @SWG\Property(property="PAID_EXPIRE", type="string"),
 * )
 */

class Room extends Model {

  protected $primaryKey = 'ROOMID';

  protected $visible = [ 'DD_GAME_ID', 'ROOMID', 'ROOMNAME', 'PORT', 'PLAYER_COUNT', 'TYPE', 'ROOMNAMETR', 'roomTypeDetails', 'MARKET_ID', 'PAID_EXPIRE'];

  public function scopeSearch($query, $keywords)
  {
      return $query->where('ROOMNAMETR', 'LIKE', '%'.$keywords.'%');
  }

  /**
   * Get type properties of room
   */
  public function roomTypeDetails()
  {
      return $this->belongsTo('App\RoomType', 'TYPE', 'TYPE');
  }

  public function latestTransaction() {
     //return $this->hasOne('App\TransactionLog','ROOM_ID','ROOMID')->where('PRODUCT', 'LIKE', '%_oda_%')->latest('ADD_DATE');
     return $this->hasOne('App\TransactionLog','ROOM_ID','ROOMID')->latest('ADD_DATE');
  }

  public function firstTransaction() {
    return $this->hasOne('App\TransactionLog','ROOM_ID','ROOMID')->orderBy('ADD_DATE', 'asc');
  }

}