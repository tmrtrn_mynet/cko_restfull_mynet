<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Swagger\Annotations as SWG;

/**
 * @SWG\Definition(
 *     @SWG\Property(property="id", type="integer"),
 *     @SWG\Property(property="TYPE", type="integer"),
 *     @SWG\Property(property="owner_name", type="string"),
 *     @SWG\Property(property="visibility", type="boolean"),
 * )
 */

class RoomType extends Model {
  protected $table = 'room_type';
  public $timestamps = false;

  protected $hidden = ['id'];

}