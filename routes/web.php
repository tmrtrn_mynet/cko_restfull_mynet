<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
/*
$router->group(['prefix' => 'api'], function () use ($router) {
    $router->get('authors',  ['uses' => 'AuthorController@showAllAuthors']);
    $router->get('authors/{id}', ['uses' => 'AuthorController@showOneAuthor']);
    $router->post('authors', ['uses' => 'AuthorController@create']);
    $router->delete('authors/{id}', ['uses' => 'AuthorController@delete']);
    $router->put('authors/{id}', ['uses' => 'AuthorController@update']);

    $router->get('games',  ['uses' => 'GameController@showAllGames']);
    $router->get('games/{id}', ['uses' => 'GameController@showOneGame']);
    $router->post('games', ['uses' => 'GameController@create']);
    $router->delete('games/{id}', ['uses' => 'GameController@delete']);
    $router->put('games/{id}', ['uses' => 'GameController@update']);

    $router->get('games/{game}/rooms',  ['uses' => 'RoomController@showAllRooms']);
    $router->get('games/{game}/rooms/{roomType}', ['uses' => 'RoomController@showRooms']);
    $router->get('rooms/{roomId}', ['uses' => 'RoomController@showRoomById']);
    $router->get('customData', ['uses' => 'RoomController@getCustomData']);
    $router->post('customData', ['uses' => 'RoomController@setCustomData']);

    $router->get('site', ['uses' => 'SiteController@showAllPages']);
    $router->get('site/{id}', ['uses' => 'SiteController@showOneSitePage']);
    $router->post('site', ['uses' => 'SiteController@createPage']);
    $router->put('site/{id}', ['uses' => 'SiteController@updatePage']);
    $router->delete('site/{id}', ['uses' => 'SiteController@deletePage']);

    $router->get('widget', ['uses' => 'SiteController@showAllWidgets']);
    $router->post('widget', ['uses' => 'SiteController@createWidget']);
    $router->get('widget/{id}', ['uses' => 'SiteController@getOneWidget']);
    $router->put('widget/{id}', ['uses' => 'SiteController@updateWidget']);
    $router->delete('widget/{id}', ['uses' => 'SiteController@deleteWidget']);

    $router->get('productgroups',  ['uses' => 'Product\GroupController@showAll']);
    $router->get('productgroups/{name}', ['uses' => 'Product\GroupController@show']);
    $router->post('productgroups', ['uses' => 'Product\GroupController@create']);
    $router->put('productgroups/{name}', ['uses' => 'Product\GroupController@update']);

    $router->get('product',  ['uses' => 'Product\ProductController@showAll']);
    $router->get('product/{sku}', ['uses' => 'Product\ProductController@show']);
    $router->post('product', ['uses' => 'Product\ProductController@create']);
    $router->put('product/{sku}', ['uses' => 'Product\ProductController@update']);

    $router->get('purchase',  ['uses' => 'Product\PurchaseController@showAll']);
    $router->post('purchase', ['uses' => 'Product\PurchaseController@create']);
    $router->put('purchase/{token}', ['uses' => 'Product\PurchaseController@updatePurchasedItem']);
    $router->get('validpurchase/{belongsTo}',  ['uses' => 'Product\PurchaseController@validpurchase']);
    $router->get('validpurchasesByTokens',  ['uses' => 'Product\PurchaseController@validpurchasesByTokens']);

    $router->get('users',  ['uses' => 'User\UserController@showAll']);
    $router->get('users/{username}/customData',  ['uses' => 'User\UserController@getCustomData']);
    $router->post('users/{username}/customData',  ['uses' => 'User\UserController@setCustomData']);

  }); */