<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */
$api = app('Dingo\Api\Routing\Router');

$api->version('v1', [
    'namespace' => 'App\Http\Controllers\Api\V1',
], function($api) {
    /*
    $api->post('games', [
        'as' => 'games.create',
        'uses' => 'Site\GameController@create'
    ]);
    $api->delete('games/{id}', [
        'as' => 'games.delete',
        'uses' => 'Site\GameController@delete'
    ]);
    $api->put('games/{id}', [
        'as' => 'games.update',
        'uses' => 'GameController@update'
    ]); */

    
    /*
    $api->post('site', [
        'as' => 'site.store',
        'uses' => 'Site\SiteController@createPage'
    ]);
    $api->put('site/{id}', [
        'as' => 'site.update',
        'uses' => 'Site\SiteController@updatePage'
    ]);
    $api->delete('site/{id}', [
        'as' => 'site.del',
        'uses' => 'Site\SiteController@deletePage'
    ]); */

    
    /*
    $api->post('widget', [
        'as' => 'widget.store',
        'uses' => 'Site\SiteController@createWidget'
    ]);*/
    
    /*
    $api->put('widget/{id}', [
        'as' => 'widget.update',
        'uses' => 'Site\SiteController@updateWidget'
    ]);
    $api->delete('widget/{id}', [
        'as' => 'widget.del',
        'uses' => 'Site\SiteController@deleteWidget'
    ]); */

});

$api->version('v1', [
    'namespace' => 'App\Http\Controllers\Api\V1',
    'middleware' => [
        'cors',
        'serializer',
        'api.throttle',
    ],
    // each route have a limit of 100 of 1 minutes
    'limit' => 100, 'expires' => 1,
], function ($api) {
    /************************ Admin *************************/

    // OPEN ROUTES
    $api->post('admin_authorizations',[
        'as' => 'admin_authorizations.store',
        'uses' => 'AdminAuthController@store',
    ]);
    // admin list
    $api->get('admins', [
        'as' => 'admins.index',
        'uses' => 'Admin\AdminController@index',
    ]);
    // admin detail
    $api->get('admins/{username}', [
        'as' => 'admins.show',
        'uses' => 'Admin\AdminController@show',
    ]);
    // update my password, ** protects in controller
    $api->put('admin/password', [
        'as' => 'admin.password.update',
        'uses' => 'Admin\AdminController@editPassword',
    ]);
    // delete token ** protects in controller
    $api->delete('admin_authorizations/current', [
        'as' => 'admin_authorizations.destroy',
        'uses' => 'AdminAuthController@destroy',
    ]);
    // update token ** protects in controller
    $api->put('admin_authorizations/current', [
        'as' => 'admin_authorizations.update',
        'uses' => 'AdminAuthController@update',
    ]);

    // PROTECTED ADMIN ROUTES
    $api->group(['middleware' => ['auth:admin']], function ($api) {
        $api->get('admin/', [
            'as' => 'admins.adminShow',
            'uses' => 'Admin\AdminController@adminShow',
        ]);

         // creates an achievement
        $api->post('admin/achievements', [
            'as' => 'achievements.store',
            'uses' => 'Achievement\AchievementController@store',
        ]);
        // delete a achievement
        $api->delete('admin/achievements/{code}', [
            'as' => 'achievements.destroy',
            'uses' => 'Achievement\AchievementController@destroy',
        ]);
        // move up achievement
        $api->post('admin/achievements/moveup/{code}', [
            'as' => 'achievements.moveup',
            'uses' => 'Achievement\AchievementController@moveup'
        ]);
        // move down achievement
        $api->post('admin/achievements/movedown/{code}', [
            'as' => 'achievements.movedown',
            'uses' => 'Achievement\AchievementController@movedown'
        ]);

        // ROOMS
        $api->get('games/{game}/rooms',  [
            'as' => 'rooms.all',
            'uses' => 'Room\RoomController@showAllRooms'
        ]);
        $api->get('games/{game}/rooms/{roomType}', [
            'as' => 'rooms.bytype',
            'uses' => 'Room\RoomController@showRooms'
        ]);
        $api->get('rooms/{roomId}', [
            'as' => 'rooms.get',
            'uses' => 'Room\RoomController@showRoomById'
        ]);

        $api->get('productgroups',  [
            'as' => 'productgroup.all',
            'uses' => 'Product\GroupController@showAll'
        ]);
        $api->get('productgroups/{name}', [
            'as' => 'productgroup.name',
            'uses' => 'Product\GroupController@show'
        ]);
        $api->post('productgroups', [
            'as' => 'productgroup.store',
            'uses' => 'Product\GroupController@create'
        ]);
        $api->put('productgroups/{name}', [
            'as' => 'productgroup.update',
            'uses' => 'Product\GroupController@update'
        ]);
    
        $api->get('product',  [
            'as' => 'product.all',
            'uses' => 'Product\ProductController@showAll'
        ]);
        $api->get('product/{sku}', [
            'as' => 'product.sku',
            'uses' => 'Product\ProductController@show'
        ]);
        $api->post('product', [
            'as' => 'product.sku',
            'uses' => 'Product\ProductController@create'
        ]);
        $api->put('product/{sku}', [
            'as' => 'product.update',
            'uses' => 'Product\ProductController@update'
        ]);

        $api->get('purchase',  [
            'as' => 'purchase.all',
            'uses' => 'Product\PurchaseController@showAll'
        ]);

    }); // END PROTECTED ADMIN ROUTES

    /************************** End Admin *****************************/

    /************************ Users *************************/
    // OPEN ROUTES
    $api->get('games',  [
        'as' => 'games.all',
        'uses' => 'Site\GameController@showAllGames'
    ]);
    $api->get('games/{id}', [
        'as' => 'games.show',
        'uses' => 'Site\GameController@showOneGame'
    ]);
    $api->get('site', [
        'as' => 'site.all',
        'uses' => 'Site\SiteController@showAllPages'
    ]);
    $api->get('site/{id}', [
        'as' => 'site.id',
        'uses' => 'Site\SiteController@showOneSitePage'
    ]);
    $api->get('widget', [
        'as' => 'widget.all',
        'uses' => 'Site\SiteController@showAllWidgets'
    ]);
    $api->get('widget/{id}', [
        'as' => 'widget.id',
        'uses' => 'Site\SiteController@getOneWidget'
    ]);

    // login
    $api->post('authorizations', [
        'as' => 'authorizations.store',
        'uses' => 'AuthController@store',
    ]);

    // user list
    $api->get('users', [
        'as' => 'users.index',
        'uses' => 'UserController@index',
    ]);
    // user detail
    $api->get('users/{id}', [
        'as' => 'users.show',
        'uses' => 'UserController@show',
    ]);

    // achievement list
    $api->get('achievements', [
        'as' => 'achievements.index',
        'uses' => 'Achievement\AchievementController@index',
    ]);
    // achievement detail
    $api->get('achievements/{code}', [
        'as' => 'achievements.show',
        'uses' => 'Achievement\AchievementController@show',
    ]);

    // PROTECTED USER ROUTES
    $api->group(['middleware' => 'api.auth'], function ($api) {

        // logout
        $api->delete('authorizations/current', [
            'as' => 'authorizations.destroy',
            'uses' => 'AuthController@destroy',
        ]);
        // update token
        $api->put('authorizations/current', [
            'as' => 'authorizations.update',
            'uses' => 'AuthController@update',
        ]);

        // my detail
        $api->get('user', [
            'as' => 'user.show',
            'uses' => 'UserController@userShow',
        ]);

        // ACHIEVEMENT
        // update user's achievement progress by group
        $api->post('achievements/groups/{group}', [
            'as' => 'achievements.group.progress',
            'uses' => 'Achievement\AchievementController@groupProgress'
        ]);
        // get user's achievement progress
        $api->get('user/achievements', [
            'as' => 'user.achievements',
            'uses' => 'Achievement\AchievementController@userIndex'
        ]);

        $api->post('user/achievements/consume', [
            'as' => 'user.achievements.consume',
            'uses' => 'Achievement\AchievementController@consumeReward'
        ]);

        // Custom Data
        /*
        $api->get('customData', [
            'as' => 'rooms.getcustomdata',
            'uses' => 'Room\RoomController@getCustomData'
        ]);
        $api->post('customData', [
            'as' => 'rooms.sendcustomdata',
            'uses' => 'Room\RoomController@setCustomData'
        ]); */

        // TODO /validpurchase/{belongsTo}

        // rooms/{roomId}/customData - alinan token ile odanin sahibi-owner eslesiyorsa ok
        $api->get('rooms/{roomId}/customData', [
            'as' => 'rooms.getcustomData',
            'uses' => 'Room\RoomController@getCustomData'
        ]);
        $api->post('rooms/{roomId}/customData', [
            'as' => 'rooms.sendcustomdata',
            'uses' => 'Room\RoomController@setCustomData'
        ]);

        $api->get('rooms/{roomId}/validpurchase', [
            'as' => 'rooms.validpurchase',
            'uses' => 'Room\RoomController@validpurchase'
        ]);

    });
    /******************** End Users *************************/

    // OPEN ROUTES WITH SIMPLE PROTECTION
    $api->post('purchase', [
        'as' => 'purchase.store',
        'uses' => 'Product\PurchaseController@create'
    ]);
    $api->put('purchase/{token}', [
        'as' => 'purchase.update',
        'uses' => 'Product\PurchaseController@updatePurchasedItem'
    ]);
    $api->get('validpurchase/{belongsTo}', [
        'as' => 'purchase.validpurchase',
        'uses' => 'Product\PurchaseController@validpurchase'
    ]);
    $api->get('validpurchasesByTokens',  [
        'as' => 'purchase.validpurchasesbytokens',
        'uses' => 'Product\PurchaseController@validpurchasesByTokens'
    ]);

    $api->get('groupProgressForUser',  [
        'as' => 'achievements.group.groupProgressForUser',
        'uses' => 'Achievement\AchievementController@groupProgressForUser'
    ]);
});
